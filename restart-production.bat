@ECHO OFF

ECHO Restarting application pool...

SET appName="FishCamp"
set serverName="DSAWEB2"

REM SET deployBinary="C:\Program Files (x86)\IIS\Microsoft Web Deploy V3\msdeploy.exe"
REM %deployBinary% -verb:sync -source:recycleApp="%appName%",recycleMode="RecycleAppPool",wmsvc="%serverName%",authType="NTLM" -dest:auto,computerName="%serverName%" -allowUntrusted

%deployBinary% -verb:sync -source:recycleApp -dest:recycleApp="%appName%",recycleMode="RecycleAppPool",wmsvc="%serverName%",authType='NTLM' -allowUntrusted
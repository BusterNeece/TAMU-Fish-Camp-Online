<?php
require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();


$db_info = $config->application->resources->doctrine->conn->toArray();
$db_info['username'] = $db_info['user'];
unset($db_info['driver'], $db_info['user']);

$new_db = Zend_Db::factory('Pdo_Mysql', $db_info);

$db_info['dbname'] = 'fishcamp_temp';
$old_db = Zend_Db::factory('Pdo_Mysql', $db_info);

/**
 * User Migration
 */

// Clear existing freshmen.
$users_with_roles_raw = $new_db->fetchAll('SELECT ur.* FROM user_has_role ur');
$users_with_roles = array();
foreach($users_with_roles_raw as $user_role)
{
	$users_with_roles[] = $user_role['user_id'];
}

// $new_db->delete('user', 'id NOT IN ('.$new_db->quote($users_with_roles).') AND fc_app_type = '.FC_TYPE_FRESHMAN);

// Pull new freshmen.
$user_id_mapping = array();
$old_users = $old_db->fetchAll('SELECT u.* FROM users AS u WHERE u.fc_app_type = ? ORDER BY u.lastname ASC', array(FC_TYPE_FRESHMAN));

foreach($old_users as $user)
{
    $old_user_id = $user['user_id'];
    
    $new_user = $new_db->fetchRow('SELECT u.* FROM user AS u WHERE u.uin = ?', array($user['uin']));
    
    $import_info = $user;
    $import_info['parentaddr'] = $import_info['parentaddr1'];
    unset($import_info['user_id'], $import_info['parentaddr1']);
    
    if ($new_user)
    {
        //$new_db->update('user', $import_info, 'uin = '.$user['uin']);
        $user_id_mapping[$old_user_id] = $new_user['id'];
    }
    else
    {
        $new_db->insert('user', $import_info);
        $user_id_mapping[$old_user_id] = $new_db->lastInsertId();
    }
}

/**
 * TouchNet Transaction Migration
 */

// Clear existing transactions.
$new_db->delete('ledger_log');
$new_db->delete('ledger');
$new_db->delete('touchnet');

// Pull new transactions.
$old_transactions = $old_db->fetchAll('SELECT u.* FROM upay AS u WHERE u.payment_status = ?', 'success');

foreach($old_transactions as $trans)
{
    // Create touchnet record.
    $new_record = $trans;
    $new_record = array(
        'trans_id'          => $trans['transaction_id'],
        'user_id'           => $user_id_mapping[$trans['local_record_id']],
    );
    
    unset($new_record['id'], $new_record['transaction_id'], $new_record['local_record_id']);
    
    $new_db->insert('touchnet', $new_record);
    $new_record_id = $new_db->lastInsertId();
    
    // Create full payment ledger record.
	$ledger_record = array(
        'ledger_item_type_id' => 8,
        'amount'        => 160.00,
        'time_created'  => time(),
        'time_updated'  => time(),
		'touchnet_id'	=> $new_record_id,
		'user_id'		=> $new_record['user_id'],
		'submitter'		=> 'System',
		'payment_method' => 'credit',
        'post_date'     => time(),
        'is_posted_to_famis' => 1,
	);
    
    $new_db->insert('ledger', $ledger_record);
    
    // Create service charge ledger record.
    $ledger_record['amount'] = 0.50;
    $ledger_record['ledger_item_type_id'] = 3;
    
    $new_db->insert('ledger', $ledger_record);
}

/**
 * Road Trip Form Migration
 */

// Clear existing road trips.
$new_db->delete('roadtrip_form');

// Fetch old road trips.
$old_forms = $old_db->fetchAll('SELECT rf.* FROM roadtrip_forms AS rf');

foreach($old_forms as $form)
{
    $form_data = unserialize($form['app_data']);
    
    $new_record = $form;
    $new_record['form_data'] = json_encode($form_data);
    unset($new_record['app_data'], $new_record['id']);
    
    $new_db->insert('roadtrip_form', $new_record);
}
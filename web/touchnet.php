<?php
/**
 * TouchNet postback processing file.
 */

require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

error_reporting(E_ALL & ~E_NOTICE);

use \Entity\Ledger;
use \Entity\LedgerItem;
use \Entity\LedgerItemType;
use \Entity\User;

try
{
    $transaction_record = \DF\Service\TouchNet::processPostData();

    // Transaction record only returned in the case of successful transactions.
    if ($transaction_record)
    {
    	$em = \Zend_Registry::get('em');
        
        $user = $transaction_record->user;
    	
    	$transaction_details = array(
    		'touchnet'			=> $transaction_record,
    		'user'				=> $user,
    		'submitter'			=> 'System',
    		'payment_method' 	=> 'credit',
            'post_date'     	=> time(),
            'is_posted_to_famis' => 1,
    	);
        
        /**
         * Post full payment.
         */
        
        $full_payment = LedgerItemType::find(8);
        $full_payment_amount = $full_payment->amounts[0]->amount;
        
    	$full_payment_record = new Ledger();
    	$full_payment_record->fromArray($transaction_details);
    	$full_payment_record->type = $full_payment;
        $full_payment_record->amount = $full_payment_amount;
        $em->persist($full_payment_record);
        
        /**
         * Post service charge.
         */
        
        $service_charge = LedgerItemType::find(3);
        $service_charge_amount = $service_charge->amounts[0]->amount;
        
    	$service_charge_record = new Ledger();
    	$service_charge_record->fromArray($transaction_details);
    	$service_charge_record->type = $service_charge;
        $service_charge_record->amount = $service_charge_amount;
        $em->persist($service_charge_record);
        $em->flush();
    }
}
catch(\Exception $exception)
{
    $error_data = \DF\Utilities::print_r(array(
        'environment' => DF_APPLICATION_ENV,
        'message' => $exception->getMessage(),
        'exception' => get_class($exception),
        'file' => $exception->getFile(),
        'stack_trace' => $exception->getTraceAsString(),
        'ip' => $_SERVER['REMOTE_ADDR'],
        'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        'request_uri' => $_SERVER['REQUEST_URI'],
        'referrer' => $_SERVER['HTTP_REFERRER'],
        'request_params' => $_REQUEST,
    ), TRUE);

    file_put_contents('touchnet.txt', $error_data."\n\n", FLAG_APPEND);
}
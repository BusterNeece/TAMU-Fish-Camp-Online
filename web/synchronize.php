<?php
require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
ini_set('memory_limit', -1);

$fc->runCampAssignment();
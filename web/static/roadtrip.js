/**
 * Report Custom Functionality
 */

var current_item;
var total_items;

$(function() {

	// Find the number of reporter fields.
	total_items = 0;
	var highest_completed = 1;
	$('input.itinerary_field').each(function() {
		var current_rel = parseInt($(this).attr('rel'));
		if (current_rel > total_items)
			total_items = current_rel;

		if ($(this).val() != '' && current_rel > highest_completed)
			highest_completed = current_rel;
	});

	// Wrap each group of reporter info.
	for(i = 1; i <= total_items; i++)
	{
		var wrapper_div = $('<div />').addClass('itinerary_group row itinerary_group_'+i).attr('rel', i);
		var fields = $('input.itinerary_field_'+i);
		
		fields.filter('.full-width').closest('div').addClass('span8');
		fields.filter('.half-width').removeClass('half-width').addClass('full-width').closest('div').addClass('span3');
		fields.closest('div').wrapAll(wrapper_div);
	}

	// Hide all groups with nothing in them (or only show the first group if empty).
	for(j = highest_completed+1; j <= total_items; j++)
	{
		$('div.itinerary_group_'+j).hide();
	}

	current_item = 1;

	// Add button.
	var button = $('<button>').attr('id', 'btn-add-itinerary').addClass('ui-button btn small mid-form success').attr('type', 'button').text('Add Itinerary Item');
	button.click(function(e) {
		e.preventDefault();
		current_item++;
		$('div.itinerary_group_'+current_item).fadeIn('fast');

		if (current_item == total_items)
			$(this).hide();
	});
	$('fieldset#itinerary').append(button).find('#btn-add-itinerary').wrap('<div class="buttons well" />');
});
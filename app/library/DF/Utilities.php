<?php
/**
 * Miscellaneous Utilities Class
 **/

namespace DF;

class Utilities
{
	const PASSWORD_LENGTH = 9;
	
	// Replacement for print_r.
	public static function print_r($var, $return = FALSE)
	{
		$return_value = '<pre style="font-size: 13px; font-family: Consolas, Courier New, Courier, monospace; color: #000; background: #EFEFEF; border: 1px solid #CCC; padding: 5px;">';
		$return_value .= print_r($var, TRUE);
		$return_value .= '</pre>';
		
		if ($return)
		{
			return $return_value;
		}
		else
		{
			echo $return_value;
		}
	}
    
    /**
     * Number handling
     */
    
    public static function ceiling($value, $precision = 0) {
        return ceil($value * pow(10, $precision)) / pow(10, $precision);
    }
    public static function floor($value, $precision = 0) {
        return floor($value * pow(10, $precision)) / pow(10, $precision);
    }
    
    public static function money_format($number)
    {
		if ($number < 0)
			return '-$'.number_format(abs($number), 2);
		else
			return '$'.number_format($number, 2);
	}
	
	public static function getFiscalYear($timestamp = NULL)
	{
		if ($timestamp === NULL)
			$timestamp = time();
		
		$fiscal_year = intval(date('Y', $timestamp));
		$fiscal_month = intval(date('m', $timestamp));
		
		if ($fiscal_month >= 9)
			$fiscal_year++;
		return $fiscal_year;
	}
	
	/**
	 * Security
	 */
	public static function generatePassword($char_length = self::PASSWORD_LENGTH)
	{
		// String of all possible characters. Avoids using certain letters and numbers that closely resemble others.
		$numeric_chars = str_split('234679');
		$uppercase_chars = str_split('ACDEFGHJKLMNPQRTWXYZ');
		$lowercase_chars = str_split('acdefghjkmnpqrtwxyz');
		
		$chars = array($numeric_chars, $uppercase_chars, $lowercase_chars);
		
		$password = '';
		for($i = 1; $i <= $char_length; $i++)
		{
			$char_array = $chars[$i % 3];
			$password .= $char_array[mt_rand(0, count($char_array)-1)];
		}
		
		return str_shuffle($password);
	}
	
	// Get the plain-english value of a given timestamp.
	public static function timeToText($timestamp)
	{
		return self::timeDifferenceText(0, $timestamp);
	}
	
	// Get the plain-english difference between two timestamps.
	public static function timeDifferenceText($timestamp1, $timestamp2)
	{
		$time_diff = abs($timestamp1 - $timestamp2);
		$diff_text = "";
		
		if ($time_diff < 60)
		{
			$time_num = intval($time_diff);
			$time_unit = 'second';
		}
		else if ($time_diff >= 60 && $time_diff < 3600)
		{
			$time_num = round($time_diff / 60, 1);
			$time_unit = 'minute';
		}
		else if ($time_diff >= 3600 && $time_diff < 216000)
		{
			$time_num = round($time_diff / 3600, 1);
			$time_unit = 'hour';
		}
		else if ($time_diff >= 216000 && $time_diff < 10368000)
		{
			$time_num = round($time_diff / 86400);
			$time_unit = 'day';
		}
		else
		{
			$time_num = round($time_diff / 2592000);
			$time_unit = 'month';
		}
		
		$diff_text = $time_num.' '.$time_unit.(($time_num != 1)?'s':'');
		
		return $diff_text;
	}

	/**
     * Truncate text (adding "..." if needed)
     */
    public static function truncateText($text, $limit = 80, $pad = '...')
    {
        if (strlen($text) <= $limit)
        {
            return $text;
        }
        else
        {
            $wrapped_text = wordwrap($text, $limit, "{N}", TRUE);
            $shortened_text = substr($wrapped_text, 0, strpos($wrapped_text, "{N}"));
            
            // Prevent the padding string from bumping up against punctuation.
            $punctuation = array('.',',',';','?','!');
            if (in_array(substr($shortened_text, -1), $punctuation))
            {
                $shortened_text = substr($shortened_text, 0, -1);
            }
            
            return $shortened_text.$pad;
        }
    }
    
    /**
     * Array Combiner (useful for configuration files)
     */
    public static function pairs($array)
    {
		return array_combine($array, $array);
    }
    
    /**
     * Unserialize and Serialized Array Repair Tool
     */
    
    public static function unserialize($serialized)
    {
		$data = @unserialize($serialized);
		
		if ($data === false && strlen($serialized) > 0)
			$data = self::repairSerializedArray($serialized);
		
		return $data;
	}
	
	public static function repairSerializedArray($serialized)
	{
		$tmp = preg_replace('/^a:\d+:\{/', '', $serialized);
		return self::repairSerializedArray_R($tmp);
	}
	
	protected static function repairSerializedArray_R(&$broken)
	{
		$data       = array();
		$index      = null;
		$len        = strlen($broken);
		$i          = 0;
	 
		while(strlen($broken))
		{
			$i++;
			if ($i > $len)
			{
				break;
			}
	 
			if (substr($broken, 0, 1) == '}') 
			{
				$broken = substr($broken, 1);
				return $data;
			}
			else
			{
				$bite = substr($broken, 0, 2);
				switch($bite)
				{   
					case 's:': // key or value
						$re = '/^s:\d+:"([^\"]*)";/';
						if (preg_match($re, $broken, $m))
						{
							if ($index === null)
							{
								$index = $m[1];
							}
							else
							{
								$data[$index] = $m[1];
								$index = null;
							}
							$broken = preg_replace($re, '', $broken);
						}
					break;
	 
					case 'i:': // key or value
						$re = '/^i:(\d+);/';
						if (preg_match($re, $broken, $m))
						{
							if ($index === null)
							{
								$index = (int) $m[1];
							}
							else
							{
								$data[$index] = (int) $m[1];
								$index = null;
							}
							$broken = preg_replace($re, '', $broken);
						}
					break;
	 
					case 'b:': // value only
						$re = '/^b:[01];/';
						if (preg_match($re, $broken, $m))
						{
							$data[$index] = (bool) $m[1];
							$index = null;
							$broken = preg_replace($re, '', $broken);
						}
					break;
	 
					case 'a:': // value only
						$re = '/^a:\d+:\{/';
						if (preg_match($re, $broken, $m))
						{
							$broken         = preg_replace('/^a:\d+:\{/', '', $broken);
							$data[$index]   = self::repairSerializedArray_R($broken);
							$index = null;
						}
					break;
	 
					case 'N;': // value only
						$broken = substr($broken, 2);
						$data[$index]   = null;
						$index = null;
					break;
				}
			}
		}
	 
		return $data;
	}
}
<?php
namespace DF\File;

class Csv extends \DF\File
{
	public static function fetch($file_name)
	{
		@ini_set('auto_detect_line_endings', 1);
		
		$csv_data = array();
		$handle = fopen(self::getFilePath($file_name), "r");
		while (($data = fgetcsv($handle)) !== FALSE)
		{
			$csv_data[] = $data;
		}
		
		fclose($handle);
		return $csv_data;
	}
	
	public static function clean($file_name)
	{
		$csv_data = self::fetch($file_name);
		$clean_data = array();
		
		if ($csv_data)
		{
			$headers = array();
			$row_num = 0;
            $col_num = 0;
            
            $header_row = array_shift($csv_data);
            foreach($header_row as $csv_col)
            {
                $field_name = strtolower(preg_replace("/[^a-zA-Z0-9_]/", "", $csv_col));
                if (!empty($field_name))
                    $headers[$col_num] = $field_name;
                $col_num++;
            }
            
			foreach($csv_data as $csv_row)
			{
				$col_num = 0;
				$clean_row = array();
				foreach($csv_row as $csv_col)
				{
					$col_name = (isset($headers[$col_num])) ? $headers[$col_num] : $col_num;
					$clean_row[$col_name] = $csv_col;
					$col_num++;
				}
                
                $clean_data[] = $clean_row;
				$row_num++;
			}
		}
		
		return $clean_data;
	}
}
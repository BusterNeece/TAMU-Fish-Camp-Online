<?php
/** 
 * GPR Appeal - Form Configuration
 */

// Constants for the form's overall state.
define('APPEAL_PHASE_INITIAL', 		0);
define('APPEAL_PHASE_INFO',			5);
define('APPEAL_PHASE_COMMITTEE', 	7);
define('APPEAL_PHASE_DECISION',		9);
define('APPEAL_PHASE_FINAL', 		14);
define('APPEAL_PHASE_COMPLETE', 	15);
define('APPEAL_PHASE_ARCHIVED', 	20);

// Constants for the final decision.
define('APPEAL_DECISION_GRANT', 	1);
define('APPEAL_DECISION_GRANT_WITH_STIPULATIONS', 2);
define('APPEAL_DECISION_DENY', 		3);
define('APPEAL_DECISION_DENY_EXTENUATING', 4);
define('APPEAL_DECISION_ABSTAIN',   5);

$decisions = array(
	APPEAL_DECISION_GRANT			=> 'Granted',
	APPEAL_DECISION_GRANT_WITH_STIPULATIONS => 'Granted with Stipulations',
	APPEAL_DECISION_DENY			=> 'Denied',
	// APPEAL_DECISION_DENY_EXTENUATING => 'Denied with Extenuating Circumstances',
);

$committee_decisions = $decisions + array(
	APPEAL_DECISION_ABSTAIN			=> 'Abstain from Decision',
);

return array(
	// Form phases in text form
	'phases' => array(
		APPEAL_PHASE_INITIAL			=> 'Incomplete',
		APPEAL_PHASE_INFO				=> 'Info',
		APPEAL_PHASE_COMMITTEE			=> 'Committee',
		APPEAL_PHASE_DECISION			=> 'Manager',
		APPEAL_PHASE_FINAL				=> 'Final',
		APPEAL_PHASE_COMPLETE			=> 'Complete',
		APPEAL_PHASE_ARCHIVED			=> 'Archive',
	),
	
	// Final appeal decisions in text form
	'decisions'		=> $decisions,
	
	// Alternate version of the item above
	'committee_decisions' => $committee_decisions,

	// Default contact if missing address
	'default_email' => array('info@stuact.tamu.edu'),
);
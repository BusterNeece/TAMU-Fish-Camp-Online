<?php
/**
 * Appeals Core Controller
 */

use \Entity\AppealForm;

class Forms_AppealsController extends \DF\Controller\Action
{
	protected $_settings;

    public function permissions()
    {
    	return $this->acl->isAllowed('is logged in');
    }

    public function preDispatch()
    {
    	$this->_settings = AppealForm::getSettings();
    	$this->view->form_settings = $this->_settings;

    	parent::preDispatch();
    }

    /**
     * General Pages (No Form)
     */
    
    public function indexAction()
    {
    	$user = $this->auth->getLoggedInUser();

		$forms = AppealForm::getRepository()->findBy(array('user_id' => $user->id));
		$this->view->forms = $forms;
    }

    public function adminAction()
    {
    	$this->acl->checkPermission(array(
    		'appeal form administrator',
    		'appeal form manager',
    		'appeal form reviewer',
    		'appeal form final',
	    ));

	    $qb = $this->em->createQueryBuilder()
	    	->select('a.id, a.password, a.phase, a.created_at, a.updated_at, u.firstname, u.lastname')
	    	->from('Entity\AppealForm', 'a')
	    	->innerJoin('a.user', 'u')
	    	->orderBy('a.id', 'DESC');
	    
	    if (!$this->acl->isAllowed('appeal form administrator'))
	    {
	    	if ($this->acl->isAllowed('appeal form manager'))
	    	{
	    		$qb->andWhere('a.phase != :phase')->setParameter('phase', APPEAL_PHASE_INFO);
	    	}
		    elseif ($this->acl->isAllowed('appeal form reviewer'))
		    {
		    	$reviewer_mode = TRUE;
		    	$qb->addSelect('a.admin_data');
		    	$qb->andWhere('a.phase = :phase')->setParameter('phase', APPEAL_PHASE_COMMITTEE);
		    }
		    elseif ($this->acl->isAllowed('appeal form final'))
		    {
		    	$qb->andWhere('a.phase = :phase')->setParameter('phase', APPEAL_PHASE_FINAL);
		    }
		}
	    
	    $forms = $qb->getQuery()->getArrayResult();
	    $forms_by_phase = array();

	    $user = $this->auth->getLoggedInUser();
		$user_id = $user->id;

	    foreach((array)$forms as $form)
	    {
	    	if ($reviewer_mode)
	    		$form['has_submitted_decision'] = isset($form['admin_data']['decisions'][$user_id]);

	    	$form['phase_text'] = $this->_settings['phases'][$form['phase']];
	    	$forms_by_phase[$form['phase']][] = $form;
	    }

	    $this->view->forms_by_phase = $forms_by_phase;
    }

    public function notifyAction()
    {
    	$this->acl->checkPermission('appeal form administrator');

    	$phase = $this->_getParam('phase');

		$forms = $this->em->createQuery('SELECT af FROM Entity\AppealForm af WHERE af.phase = :phase')
			->setParameter('phase', $phase)
			->getResult();

		$notified = array();
		foreach($forms as $form)
		{
			$email = $form->notify();
			$notified[] = \DF\Utilities::print_r($email['to'], TRUE);
		}

		$this->alert('<b>Notifications sent to:</b><br>'.implode('<br>', $notified), 'green');
		$this->redirectFromHere(array('action' => 'index'));
		return;
    }

	/**
	 * Form-Specific Actions
	 */
	
	protected function _getForm($required = TRUE)
	{
		if ($this->_hasParam('id'))
		{
			$password = $this->_getParam('id');
			$form = AppealForm::getRepository()->findOneBy(array('password' => $password));
		}
		else
		{
			$form = NULL;
		}

		if ($form instanceof AppealForm)
			return $form;
		else if ($required)
			throw new \DF\Exception\DisplayOnly('The form specified could not be found.');
		else
			return NULL;
	}

	public function newAction()
	{
		$form_config = $this->current_module_config->forms->appeal->form->toArray();
		$form = new \DF\Form($form_config);

		$user = $this->auth->getLoggedInUser();
		$form->setDefaults(array(
			'name'		=> $user->firstname.' '.$user->lastname,
			'uin'		=> $user->uin,
		));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$record = new AppealForm;
			$record->request_data = $data;
			$record->user = $this->auth->getLoggedInUser();
			$record->save();

			if ($data['appeal_reason'] == "attendance")
				$record->setPhase(APPEAL_PHASE_COMMITTEE);
			else
				$record->setPhase(APPEAL_PHASE_INFO);

			$this->alert('<b>Appeal request submitted.</b><br>You will be updated by e-mail when your request has been processed.', 'green');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$this->view->form = $form;
	}

	public function resultsAction()
	{
		$record = $this->_getForm();

		if ($record->phase != APPEAL_PHASE_COMPLETE)
			throw new \DF\Exception\DisplayOnly('Cannot View Form Results: This form is not currently open to viewing results.');
		
		$this->view->record = $record;
	}

	public function viewAction()
	{
		$this->acl->checkPermission(array(
			'appeal form administrator',
			'appeal form manager',
			'appeal form reviewer',
			'appeal form final',
		));

		// Auto-redirect for other permissions.
		if (!$this->acl->isAllowed(array('appeal form administrator', 'appeal form manager')))
		{
			if ($this->acl->isAllowed('appeal form reviewer'))
				$this->redirectFromHere(array('action' => 'committee'));
			elseif ($this->acl->isAllowed('appeal form final'))
				$this->redirectFromHere(array('action' => 'final'));
			return;
		}

		$record = $this->_getForm();

		// Pre-populate some information from COMPASS before showing the page.
		if ($record->phase == APPEAL_PHASE_INFO)
		{
			$this->acl->checkPermission('appeal form administrator');

			$grade_data_raw = \FCO\Grades::fetchGradesForAllTerms($record->user->uin);
			$grades = array();

			foreach($grade_data_raw['grades'] as $term_num => $term_gpr)
			{
				$term_name = \FCO\Grades::getTermName($term_num);
				$grades[$term_name] = number_format($term_gpr, 2);
			}

			$this->view->grades = $grades;
		}
		
		if (isset($_REQUEST['form']))
		{
			switch($record->phase)
			{
				case APPEAL_PHASE_INFO:
					$admin_data = array_merge((array)$record->admin_data, $_REQUEST['form']);
					$record->admin_data = $admin_data;
					$record->save();

					$record->setPhase(APPEAL_PHASE_COMMITTEE);

					$this->alert('<b>Form information submitted and sent to the committee for review.</b>', 'green');
				break;
					
				case APPEAL_PHASE_DECISION:
					$admin_data = array_merge((array)$record->admin_data, $_REQUEST['form']);
					$admin_data['decision_text'] = $this->_settings['decisions'][$admin_data['decision']];

					$record->review_final = $admin_data['decision'];
					$record->admin_data = $admin_data;
					$record->save();

					$data = (array)$record->request_data;
					if ($data['appeal_reason'] == "attendance")
						$record->setPhase(APPEAL_PHASE_COMPLETE);
					else
						$record->setPhase(APPEAL_PHASE_FINAL);
					
					$this->alert('<b>Form saved and sent to final review.</b>', 'green');
				break;
			}

			$this->redirectFromHere(array('form' => NULL));
			return;
		}
		else
		{
			switch($record->phase)
			{
				case APPEAL_PHASE_COMMITTEE:
					// Get list of members for each group.
					$admin_data = (array)$record->admin_data;
					$committee_members = \Entity\User::getUsersWithAction('appeal form reviewer');
        			
					foreach($committee_members as &$member)
					{
						$member['has_made_decision'] = (isset($admin_data['decisions'][$member['id']]));
					}

					$this->view->committee_members = $committee_members;
				break;
			}
		}

		$form_config = $this->current_module_config->forms->appeal->form->toArray();
		$form = new \DF\Form($form_config);
		$form->populate($record->request_data);

		$this->view->record = $record;
		$this->view->form = $form;
	}

	public function committeeAction()
	{
		$record = $this->_getForm();
		$this->acl->checkPermission('appeal form reviewer');
		
		if (isset($_REQUEST['form']))
		{
			$user = $this->auth->getLoggedInUser();

			$decision = array(
				'name'				=> $user->firstname.' '.$user->lastname,
				'decision'			=> $_REQUEST['form']['decision'],
				'decision_text'		=> $this->_settings['committee_decisions'][$_REQUEST['form']['decision']],
				'comments'			=> $_REQUEST['form']['comments'],
			);

			$admin_data = (array)$record->admin_data;
			$admin_data['decisions'][$user->id] = $decision;

			$record->admin_data = $admin_data;
			$record->save();

			$number_of_reviewers = count(\Entity\User::getUsersWithAction('appeal form reviewer'));
			$number_of_decisions = count($admin_data['decisions']);
			
			if ($number_of_reviewers <= $number_of_decisions)
				$record->setPhase(APPEAL_PHASE_DECISION);
			
			$this->alert('<b>Decision successfully submitted.</b>', 'green');
			$this->redirectFromHere(array('action' => 'admin', 'id' => NULL));
			return;
		}
		else
		{
			$form_config = $this->current_module_config->forms->appeal->form->toArray();
			$form = new \DF\Form($form_config);
			$form->populate($record->request_data);

			$this->view->record = $record;
			$this->view->form = $form;
		}
	}

	public function finalAction()
	{
		$this->acl->checkPermission('appeal form final');
		$record = $this->_getForm();
			
		if (isset($_REQUEST['decision']))
		{
			$admin_data = (array)$record->admin_data;
			$admin_data['final_comments'] = $_REQUEST['form']['final_comments'];
			
			if ($_REQUEST['decision'] == "approve")
			{
				$record->review_final = $admin_data['decision'];
				$admin_data['decisions'] = NULL;

				$new_phase = APPEAL_PHASE_COMPLETE;
			}
			else
			{
				$admin_data['decisions'] = NULL;
				$new_phase = APPEAL_PHASE_COMMITTEE;
			}
			
			$record->admin_data = $admin_data;
			$record->save();

			$record->setPhase($new_phase);

			$this->alert('<b>Review submitted successfully.</b>', 'green');
			$this->redirectFromHere(array('action' => 'admin', 'id' => NULL));
			return;
		}
		else
		{
			$this->redirectFromHere(array('action' => 'view'));
			return;
		}
	}

	public function archiveAction()
	{
		$record = $this->_getForm();
		$record->setPhase(APPEAL_PHASE_ARCHIVED);

		$this->alert('<b>Form archived successfully.</b>', 'green');
		$this->redirectFromHere(array('action' => 'admin', 'id' => NULL));
	}
}
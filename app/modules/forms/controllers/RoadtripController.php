<?php
/**
 * Road Trip Form
 */

use \Entity\RoadtripForm;
use \Entity\RoadtripFormMessage;

class Forms_RoadtripController extends \DF\Controller\Action
{
    protected $_record_id;
    protected $_record;
    
    public function preDispatch()
    {
        if ($this->_hasParam('id'))
        {
            $this->_record_id = $this->_getParam('id');
            $this->_record = RoadtripForm::getRepository()->findOneBy(array('app_id_hash' => $this->_record_id));
            
            if (!($this->_record instanceof RoadtripForm))
                throw new \DF\Exception\DisplayOnly('Form not found!');
        }
    }
    
	public function indexAction()
	{
		if ($this->_hasParam('id'))
		{
			$this->redirectFromHere(array('action' => 'edit', 'id' => $this->_getParam('id')));
			return;
		}
	}
	
	public function editAction()
	{
		$form_config = $this->current_module_config->forms->roadtrip->form->toArray();
		unset($form_config['groups']['trip']['elements']['itinerary']);

		$form = new \DF\Form($form_config);
		
		if ($this->_record instanceof RoadtripForm)
		{
            $record = $this->_record;
            $form_data = $record['form_data'];
			$form->setDefaults($form_data);
		}
		
		if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();

            if (!$record)
                $record = new RoadtripForm();

            $record->timestamp = time();
            $record->save();

            // Handle session name conversion.
            $sess_code = substr($data['camp_name'], 0, strpos($data['camp_name'], ':'));
            list($sess_name, $sess_color) = explode(' ', $sess_code);
            $data['camp_session'] = $sess_name;
            $data['camp_color'] = $sess_color;

            // File upload handling.
			$data['excel_file'] = array();
			$email_data = $data;
			
			$access_code = $record->app_id_hash;
			$uploaded_files = $form->processFiles('roadtrip', $access_code);
			
			foreach($uploaded_files as $element_name => $element_files)
			{
				$data[$element_name] = $element_files;
				$email_data[$element_name] = array();
				foreach($element_files as $file_offset => $file_path)
				{
					$email_data[$element_name][$file_offset] = \DF\Url::content($file_path);
				}
			}
            
            $date_fields = array('departure_date', 'return_date');
            foreach($date_fields as $date_field)
            {
                $email_data[$date_field] = date('F j, Y', $data[$date_field]);
            }
			
			$record->form_data = $data;
			$record->save();
			
			// Send e-mail notification.
			$email_subject = 'Road Trip - '.$data['camp_name'].' - '.date('m/d/Y', $data['departure_date']).' to '.date('m/d/Y', $data['return_date']);
			
			$email_always = $this->current_module_config->forms->roadtrip->notify->toArray();
			$email_to = array_merge($email_always, array($data['email'], $data['partner_email']));
			
			\DF\Messenger::send(array(
				'to'		=> $email_to,
				'subject'	=> $email_subject,
				'template'	=> 'roadtrip/submitted',
                'module'    => 'forms',
				'vars'		=> array(
					'id'	=> $record->app_id_hash,
					'form'	=> $email_data,
				),
			));
			
			$this->alert('Form submitted! The form\'s ID is '.$record->app_id_hash.'. You can enter this ID into the main Roadtrip form page at any time to update the information in the application.');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
		}

		$this->view->headTitle('Fish Camp Road Trip Form');
		$this->view->headScript()->appendFile(\DF\Url::content('roadtrip.js'));
		$this->renderForm($form);
	}
    
    public function messagesAction()
    {
        $this->view->record = $this->_record;
    }
    
    public function postmessageAction()
    {
        $new_message = new RoadtripFormMessage();
        $new_message->form = $this->_record;
        $new_message->message_body = $_REQUEST['message'];
        $type = ($this->acl->isAllowed('manage road trips')) ? RoadtripFormMessage::MESSAGE_FROM_ADMIN : RoadtripFormMessage::MESSAGE_FROM_SUBMITTER;
        $new_message->message_type = $type;
        $new_message->save();
        
        $this->redirectFromHere(array('action' => 'messages'));
        return;
    }
    
    /**
     * Administration
     */
    
    public function manageAction()
	{
		$this->acl->checkPermission('manage road trips');
		
        if (isset($_REQUEST['start_date']) || isset($_REQUEST['end_date']))
        {
            $start_date = \DF\Form\Element\UnixDate::processArray($_REQUEST['start_date']);
            $end_date = \DF\Form\Element\UnixDate::processArray($_REQUEST['end_date'], time() + 86400*365);
            
            $this->redirectFromHere(array('start_date' => $start_date, 'end_date' => $end_date));
            return;
        }
        
        $qb = $this->em->createQueryBuilder()
            ->select('rf')
            ->from('\Entity\RoadtripForm', 'rf')
            ->orderBy('rf.index_startdate', 'DESC');
        
        if ($this->_hasParam('start_date') || $this->_hasParam('end_date'))
        {
            $this->view->start_date = $this->_getParam('start_date');
            $this->view->end_date = $this->_getParam('end_date');
            
            $qb->where('rf.index_enddate >= :begin AND rf.index_startdate <= :end');
            $qb->setParameters(array(
                'begin' => $this->_getParam('start_date'),
                'end' => $this->_getParam('end_date')
            ));
        }
        
        $paginator = new \DF\Paginator\Doctrine($qb);
        $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);
		$this->view->pager = $paginator;
        
        switch(strtolower($this->_getParam('format', 'html')))
        {
            case "csv":
                $this->doNotRender();
                
                $export_data = array(array(
                    'Form ID',
                    'Session',
                    'Camp Name',
                    'Camp Color',
                    'Submitter E-mail',
                    'Partner E-mail',
                    'Destination',
                    'Trip Type',
                    'Departure Date',
                    'Departure Time from CS',
                    'Arrival Time at Dest',
                    'Return Time',
                    'Departure Time from Dest',
                    'Arrival Time at CS',
                ));
                
                $all_forms = $qb->getQuery()->getArrayResult();
                foreach($all_forms as $form)
                {
                    $form = array_merge($form, $form['form_data']);
                    
                    $export_data[] = array(
                        $form['app_id_hash'],
                        $form['camp_session'],
                        $form['camp_name'],
                        $form['camp_color'],
                        $form['email'],
                        $form['partner_email'],
                        $form['destination'],
                        $form['trip_type'],
                        date('m/d/Y', $form['departure_date']),
                        $form['departure_start_time'],
                        $form['departure_end_time'],
                        date('m/d/Y', $form['return_date']),
                        $form['return_start_time'],
                        $form['return_end_time'],
                    );
                }
                
                \DF\Export::csv($export_data);
                return;
            break;
            
            case "html":
            default:
                $this->render();
                return;
            break;
        }
	}
    
    public function deleteAction()
    {
		$this->acl->checkPermission('manage road trips');
		
        $this->_record->delete();
        
        $this->alert('Form deleted successfully.');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        return;
    }
    
    public function viewAction()
    {
		$this->acl->checkPermission('manage road trips');
		
        $form = new \DF\Form($this->current_module_config->forms->roadtrip->form);
        $form->setDefaults($this->_record['form_data']);
        
        $this->view->record = $this->_record;
        $this->view->form = $form;
    }
    
    public function setnotesAction()
    {
		$this->acl->checkPermission('manage road trips');
		
        $this->_record->fromArray(array(
            'form_advisor_notes'    => $_REQUEST['notes']['advisor'],
            'form_director_notes'    => $_REQUEST['notes']['director'],
        ));
        $this->_record->save();
        
        $this->redirectFromHere(array('action' => 'view'));
        return;
    }
}
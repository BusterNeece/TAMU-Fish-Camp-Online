<?php
return array(
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(

			'uins' => array('textarea', array(
				'label' => 'Paste List of UINs',
				'description' => 'Enter your preferences for this group by pasting in the UINs of your preferred applicants. Enter each UIN on a new line, with the highest preferred applicant at the top of the list and the lowest preferred applicant at the bottom.',
				'class' => 'full-width full-height',
				'required' => true,
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save New Preferences',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);
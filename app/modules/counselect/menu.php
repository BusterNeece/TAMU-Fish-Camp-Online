<?php
return array(
    'default' => array(
        
		'counselect' => array(
			'label' => 'Counselors',
			'module' => 'counselect',
			'show_only_with_subpages' => TRUE,
            
            'pages' => array(
            
				'counselect_index' => array(
					'label' 	=> 'Pre-Counselect',
					'module' 	=> 'counselect',
					'controller' => 'index',
					'permission' => array('is chair'),
				),

				'counselect_manage' => array(
					'label' 	=> 'Manage Pre-Counselect',
					'module' 	=> 'counselect',
					'controller' => 'manage',
					'permission' => array('manage counselect'),
				),

				'counselect_live' => array(
					'label' 	=> 'Counselect Live',
					'module' 	=> 'counselect',
					'controller' => 'live',
					'permission' => array('is chair', 'manage counselect'),
				),

				'counselor_review' => array(
					'label' 	=> 'Counselor App Review',
					'module' 	=> 'counselect',
					'controller' => 'review',
					'permission' => array('is chair', 'manage counselect'),
				),
                
            ),
		),
		
    ),
);
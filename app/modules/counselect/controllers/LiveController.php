<?php
/**
 * Appeals Core Controller
 */

use \Entity\Camp;
use \Entity\Session;
use \Entity\User;
use \Entity\Settings;

class Counselect_LiveController extends \DF\Controller\Action
{
    public function permissions()
    {
        return true;
    }

    public function preDispatch()
    {
        $this->view->counselors = $this->em->createQuery('SELECT u FROM Entity\User u WHERE u.fc_app_type IN (:counselor) ORDER BY u.lastname ASC, u.firstname ASC')
            ->setParameter('counselor', array(FC_TYPE_COUNSELOR, FC_TYPE_COUNSELOR2))
            ->getArrayResult();
    }

    public function indexAction()
    {
        if ($this->acl->isAllowed('manage counselect'))
            $this->redirectFromHere(array('action' => 'manager'));
        else
            $this->redirectFromHere(array('action' => 'chair'));
        return;
    }

    public function managerAction()
    {
        $this->acl->checkPermission('manage counselect');

        $sessions = Session::fetchArray('name');
        $camps = Camp::fetchArray('name');

        $overview = array();
        foreach($sessions as $session_obj)
        {
            $session = $session_obj['name'];
            foreach($camps as $camp_obj)
            {
                $camp = $camp_obj['name'];
                $overview[$session][$camp] = $session.' '.$camp;
            }
        }

        $disabled_raw = array(
            'B Crew',
            'C Crew',
            'D Crew',
            'E Crew',
            'F Crew',
            'G Crew',
            'G Green',
            'G Red',
        );

        $disabled = array();
        foreach($disabled_raw as $dis)
        {
            $disabled[$dis] = $dis;
        }
        $this->view->disabled = $disabled;

        $this->view->overview = $overview;
    }

    public function assignAction()
    {
        $this->doNotRender();

        $user_name = $this->_getParam('name');
        $assigned_to = $this->_getParam('assigned_to');

        $uin = substr($user_name, -10, 9);
        $user = User::getOrCreate($uin);

        list($session, $camp) = explode(' ', $assigned_to);
        $user->fc_assigned_session = $session;
        $user->fc_assigned_camp = $camp;
        $user->save();

        echo \Zend_Json::encode(array('status' => 'success'));
        return;
    }

    public function chairAction()
    {
        $this->acl->checkPermission('is chair');

        $user = $this->auth->getLoggedInUser();
        if (!$user->fc_assigned_camp || !$user->fc_assigned_session)
            throw new \DF\Exception\DisplayOnly('You are not currently assigned to a session or camp. You must be assigned to a session and camp before you can submit preferences for that camp. Contact a director for assistance.');
        
        $camp_name = $user->fc_assigned_camp;
        if ($camp_name)
            $camp = Camp::getRepository()->findOneByName($camp_name);

        $session_name = $user->fc_assigned_session;
        if ($session_name)
            $session = Session::getRepository()->findOneByName($session_name);
        
        $this->view->camp = $camp;
        $this->view->session = $session;

        // Pre-counselect Preferences
        $prefs_raw = $this->em->createQuery('SELECT cs.preference, u.lastname, u.firstname, u.uin FROM Entity\Counselect cs JOIN cs.user u WHERE cs.camp = :camp AND cs.session = :session AND u.fc_app_type != :no_reg ORDER BY cs.preference ASC')
            ->setParameter('no_reg', FC_TYPE_NONE)
            ->setParameter('camp', $camp)
            ->setParameter('session', $session)
            ->getArrayResult();

        $prefs = array('No User (000000000)' => 1);
        foreach($prefs_raw as $pref)
        {
            $key = $pref['lastname'].', '.$pref['firstname'].' ('.$pref['uin'].')';
            $prefs[$key] = $pref['preference'];
        }

        $this->view->prefs = \Zend_Json::encode($prefs);
    }
}
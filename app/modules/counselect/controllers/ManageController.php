<?php
/**
 * Appeals Core Controller
 */

use \Entity\Camp;
use \Entity\Session;
use \Entity\User;
use \Entity\Counselect;
use \Entity\Settings;

class Counselect_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage counselect');
    }

    public function indexAction()
    {
        $sessions = Session::fetchArray('name');
        $camps = Camp::fetchArray('name');
        $categories = Counselect::getCategories();

        $preferences = $this->em->createQuery('SELECT cs, s, c FROM Entity\Counselect cs JOIN cs.camp c JOIN cs.session s')
            ->getArrayResult();

        $prelim = array();
        foreach($preferences as $pref)
        {
            $camp = $pref['camp']['name'];
            $session = $pref['session']['name'];
            $category = $pref['category'];

            if ($pref['status'] == Counselect::STATUS_ASSIGNED)
                $prelim[$session][$camp][$category]['assigned']++;
            
            $prelim[$session][$camp][$category]['total']++;
        }

        $report = array();
        foreach($sessions as $session_obj)
        {
            $session = $session_obj['name'];

            foreach($camps as $camp_obj)
            {
                $camp = $camp_obj['name'];
                $has_all_categories = TRUE;

                $report_row = array();

                foreach($categories as $cat_id => $cat_name)
                {
                    if (isset($prelim[$session][$camp][$cat_id]))
                    {
                        $report_row['total'] += (int)$prelim[$session][$camp][$cat_id]['total'];
                        $report_row['assigned'] += (int)$prelim[$session][$camp][$cat_id]['assigned'];
                    }
                    else
                    {
                        $has_all_categories = FALSE;
                    }
                }

                $report_row['is_complete'] = $has_all_categories;
                $report[$session][$camp] = $report_row;
            }
        }

        $this->view->report = $report;
        $this->view->categories = $categories;

        $this->view->is_open = (Settings::getSetting('counselect_unlocked', 1) == 1);
        $this->view->is_results_view = (Settings::getSetting('counselect_results_view', 1) == 1);
    }

    public function assignAction()
    {
        $mode = $this->_getParam('mode', 'standard');

        $preferences = $this->em->createQuery('SELECT cs, s, c, u FROM Entity\Counselect cs JOIN cs.camp c JOIN cs.session s JOIN cs.user u ORDER BY cs.preference ASC')
            ->getResult();
        
        usort($preferences, array($this, '_shuffle'));

        $users_by_id = array();
        foreach($preferences as $pref)
        {
            $user = $pref->user;
            $users_by_id[$user->id] = $user;
        }

        $categories = Counselect::getCategories();

        $counselors_per_cat = Counselect::getCounselorsPerCategory();
        $counselors_per_camp = 24;

        $totals = array();
        $assigned_users = array();

        if ($mode == "overflow")
        {
            foreach($preferences as $pref)
            {
                $code = $pref['session']['name'].'_'.$pref['camp']['name'];
                if ($pref['status'] == Counselect::STATUS_ASSIGNED)
                {
                    $assigned_users[$pref['user_id']] = array(
                        'skip'          => true,
                    );
                    $totals[$code]++;
                }
            }
        }

        foreach($preferences as $pref)
        {
            $pref_user = $pref->user;

            $grouping = $pref['session']['name'].'_'.$pref['camp']['name'].'_'.$pref['category'];
            $code = $pref['session']['name'].'_'.$pref['camp']['name'];

            if ($pref_user->fc_app_type == FC_TYPE_NONE)
            {
                $pref->status = Counselect::STATUS_UNASSIGNED;
            }
            else if ($pref_user->fc_app_type == FC_TYPE_COUNSELOR2 && $mode == "overflow")
            {
                if ($pref_user->fc_assigned_session == $pref['session']['name'] && $pref_user->fc_assigned_camp == $pref['camp']['name'])
                {
                    $pref->status = Counselect::STATUS_ASSIGNED;
                }
                else
                {
                    $pref->status = Counselect::STATUS_UNASSIGNED;
                }
            }
            else
            {
                if ($mode == "overflow")
                {
                    // Skip already assigned people.
                    if (isset($assigned_users[$pref['user_id']]))
                        continue;

                    if ($totals[$code] < $counselors_per_camp)
                    {
                        $pref->status = Counselect::STATUS_ASSIGNED;

                        $assigned_users[$pref['user_id']] = array(
                            'session'       => $pref['session']['name'],
                            'camp'          => $pref['camp']['name'],
                        );
                        $totals[$grouping]++;
                    }
                }
                else
                {
                    // Reject already assigned or overpopulated matches.
                    if ($totals[$grouping] >= $counselors_per_cat || isset($assigned_users[$pref['user_id']]))
                    {
                        $pref->status = Counselect::STATUS_UNASSIGNED;
                    }
                    else
                    {
                        $pref->status = Counselect::STATUS_ASSIGNED;

                        $assigned_users[$pref['user_id']] = array(
                            'session'       => $pref['session']['name'],
                            'camp'          => $pref['camp']['name'],
                        );
                        $totals[$grouping]++;
                    }
                }
            }

            $this->em->persist($pref);
        }

        foreach($users_by_id as $user_id => $user)
        {
            if (isset($assigned_users[$user_id]))
            {
                $assignment = $assigned_users[$user_id];

                if ($assignment['skip'])
                    continue;

                $user->fc_app_type = FC_TYPE_COUNSELOR2;
                $user->fc_assigned_session = $assignment['session'];
                $user->fc_assigned_camp = $assignment['camp'];
            }
            else
            {
                $user->fc_app_type = FC_TYPE_COUNSELOR;
            }

            $this->em->persist($user);
        }

        $this->em->flush();

        $this->alert('<b>Assignment completed!</b><br>'.count($assigned_users).' counselors were assigned.', 'green');
        $this->redirectFromHere(array('action' => 'index'));
        return;
    }

    protected function _shuffle($a, $b)
    {
        $a_pref = $a['preference'];
        $b_pref = $b['preference'];

        if ($a_pref < $b_pref)
            return -1;
        else if ($a_pref > $b_pref)
            return 1;
        else
            return mt_rand(-1, 1);
    }

    public function exportAction()
    {
        $export_data = array(array(
            'UIN',
            'First Name',
            'Last Name',
            'Session',
            'Camp',
            'Preference',
        ));

        $selected = $this->em->createQuery('SELECT cs, s, c, u FROM Entity\Counselect cs JOIN cs.camp c JOIN cs.session s JOIN cs.user u WHERE cs.status = :status ORDER BY s.name ASC, c.name ASC')
            ->setParameter('status', Counselect::STATUS_ASSIGNED)
            ->getArrayResult();
        
        foreach($selected as $entry)
        {
            $export_data[] = array(
                $entry['user']['uin'],
                $entry['user']['firstname'],
                $entry['user']['lastname'],
                $entry['session']['name'],
                $entry['camp']['name'],
                $entry['preference'],
            );
        }

        $this->doNotRender();
        \DF\Export::csv($export_data);
    }

    public function toggleAction()
    {
        $flag = 'counselect_'.$this->_getParam('flag', 'unlocked');

        $current_setting = (int)Settings::getSetting($flag, 1);
        $current_setting = 1 - $current_setting;

        Settings::setSetting($flag, $current_setting);

        $this->alert('<b>Counselect flag toggled.</b>', 'green');
        $this->redirectFromHere(array('action' => 'index'));
        return;
    }
}
<?php
use \Entity\User;

class Registration_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('manage registration');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
		$fc = \Zend_Registry::get('fc');
        
        if (\DF\Cache::test('registration_homepage'))
        {
            $view_variables = \DF\Cache::get('registration_homepage');
        }
        else
        {
            $view_variables = array();
            
            $statistics_interval = 3600;
            $statistics_intervals_on_graph = 9;
            
            $totals_interval = 86400;
            $totals_intervals_to_show = 50;
            $totals_start = strtotime('12:01 am -'.$totals_intervals_to_show.' days');
            
            /**
             * Session Populations Chart
             */
		    
            $view_variables['session_chart'] = $fc->getSessionChart();
            
            /**
             * User Totals and Statistics
             */
            
            $user_statistics = array(
                'all'				=> 0,
                'chair'				=> 0,
                'counselor'			=> 0,
                'counselor2'		=> 0,
                'counselor2_paid' 	=> 0,
                'counselor2_total' 	=> 0,
                'freshman'			=> 0,
                'freshman_scholarship' => 0,
                'freshman_minor'	=> 0,
                'freshman_complete' => 0,
                'freshman_total' 	=> 0,
            );
            
            $statistics_end_time = time();
            $statistics_threshold_time_raw = $statistics_end_time - ($statistics_interval * $statistics_intervals_on_graph);
            $statistics_threshold_time = intval(ceil($statistics_threshold_time_raw / $statistics_interval) * $statistics_interval);
            
            // Zero out statistics.
            $user_registration_times = array();
            for($i = 0; $i < $statistics_intervals_on_graph; $i++)
            {
                $current_reg_time = $statistics_threshold_time + ($i * $statistics_interval);
                $user_registration_times[$current_reg_time] = 0;
            }
            
            // Zero out day totals.
            $user_registrations_per_day = array();
            $user_registrations_to_date = array();
            
            for($i = 0; $i < $totals_intervals_to_show; $i++)
            {
                $current_day = $totals_start + ($i * $totals_interval);
                $current_day_abbr = date('Y-m-d', $current_day);
                
                $user_registrations_per_day[$current_day_abbr] = 0;
            }

            $raw_user_data = $this->em->createQuery('SELECT u.fc_app_type, u.fc_app_completed, u.fc_received_payment, u.fc_received_papers FROM Entity\User u ORDER BY u.fc_app_completed DESC')
                ->getArrayResult();
            
            foreach($raw_user_data as $raw_user)
            {
                $user_statistics['all']++;
                
                switch($raw_user['fc_app_type'])
                {
                    case FC_TYPE_COUNSELOR:
                        $user_statistics['counselor']++;
                        break;
                    
                    case FC_TYPE_COUNSELOR2:
                        if ($raw_user['fc_received_payment'] != 0)
                            $user_statistics['counselor2_paid']++;
                        else
                            $user_statistics['counselor2']++;
                        
                        $user_statistics['counselor2_total']++;
                        break;
                    
                    case FC_TYPE_FRESHMAN:
                        if ($raw_user['fc_app_completed'] != 0)
                        {
                            if ($raw_user['fc_received_payment'] != 0 && $raw_user['fc_received_papers'] != 0)
                                $user_statistics['freshman_complete']++;
                            else if ($raw_user['fc_received_papers'] == 0)
                                $user_statistics['freshman_minor']++;
                            else
                                $user_statistics['freshman_scholarship']++;
                        }
                        else
                        {
                            $user_statistics['freshman']++;
                        }
                        
                        $user_statistics['freshman_total']++;
                        break;
                    
                    case FC_TYPE_CHAIR:
                        $user_statistics['chair']++;
                        break;
                }
                
                if ($raw_user['fc_app_completed'] != 0)
                {	
                    $registration_day_abbr = date('Y-m-d', $raw_user['fc_app_completed']);		
                    if (isset($user_registrations_per_day[$registration_day_abbr]))
                    {
                        $user_registrations_per_day[$registration_day_abbr]++;
                    }
                    
                    $user_registration_minute = intval(floor($raw_user['fc_app_completed'] / $statistics_interval) * $statistics_interval);
                    if (isset($user_registration_times[$user_registration_minute]))
                    {
                        $user_registration_times[$user_registration_minute]++;
                    }
                }
            }
            
            $view_variables['user_statistics'] = $user_statistics;
    
            /**
             * Registration Chart
             */
            
            $registration_chart = array();
    
            $max_num_registrants = max($user_registration_times);
            $max_registrants_per_second = round($max_num_registrants / $statistics_interval, 2) * 60;
            if ($max_registrants_per_second == 0)
                $max_registrants_per_second = 1;
            
            foreach($user_registration_times as $time_range_start => $num_registrants)
            {
                $registration_chart[] = array(
                    'time'          => date('g:ia', $time_range_start),
                    'overall'       => $num_registrants,
                    'per_interval'  => round($num_registrants / $statistics_interval, 2) * 60,
                );
            }
            
            $view_variables['registration_chart'] = $registration_chart;
            
            /**
             * Other Statistics
             */
            
            foreach($user_registrations_per_day as $day_name => $user_count)
            {
                if ($user_count < 5)
                    unset($user_registrations_per_day[$day_name]);
            }
            
            $view_variables['registrations_per_day'] = $user_registrations_per_day;
            
            \DF\Cache::save($view_variables, 'registration_homepage', array(), 600);
        }
        
        $this->view->assign($view_variables);
	}
    
    public function assignmentAction()
    {
        $fc = \Zend_Registry::get('fc');
        $fc->runCampAssignment();
        
        \DF\Cache::remove('registration_homepage');
        
        $this->alert('<b>Camp auto-assignment complete!</b><br>The graph data below has automatically been updated.', 'green');
        $this->redirectFromHere(array('action' => 'index'));
    }

    public function lockassignmentAction()
    {
        $this->acl->checkPermission('administer all');

        $fc = \Zend_Registry::get('fc');
        $campers = $fc->getEligibleCampers();

        $camper_ids = array();
        foreach($campers as $camper)
            $camper_ids[] = $camper['id'];

        $lock_all = $this->em->createQuery('UPDATE Entity\User u SET u.fc_assignment_override = 1 WHERE u.id IN (:users)')
            ->setParameter('users', $camper_ids)
            ->execute();

        $this->alert('<b>All eligible freshmen locked.</b>', 'green');
        $this->redirectFromHere(array('action' => 'index'));
    }

    public function confirmationAction()
    {
        $this->acl->checkPermission('administer all');

        $fc = \Zend_Registry::get('fc');
        $campers = $fc->getEligibleCampers();

        foreach($campers as $camper)
        {
            \DF\Utilities::send(array(
                'to'        => array($camper['email'], $camper['email2']),
                'subject'   => 'Fish Camp Session Confirmation',
                'template'  => 'session_confirmation',
                'vars'      => array('student' => $camper),
            ));
        }

        $this->alert('<b>Session confirmations sent to all eligible freshmen.</b>', 'green');
        $this->redirectFromHere(array('action' => 'index'));
    }
}
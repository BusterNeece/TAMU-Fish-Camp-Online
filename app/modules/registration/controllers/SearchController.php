<?php
use \Entity\Touchnet;
use \Entity\User;

class Registration_SearchController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('manage registration');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
		// Convert form submission into URL parameter.
		if (isset($_REQUEST['criteria']))
		{
            $criteria = $_REQUEST['criteria'];
            $criteria['name'] = trim($criteria['name']);
            
			$this->redirectFromHere(array('criteria' => base64_encode(Zend_Json::encode($criteria))));
			return;
		}
		
		if ($this->_hasParam('criteria'))
		{
			$view = ($this->_hasParam('view')) ? $this->_getParam('view') : 'default';
			$this->view->view = $view;
			
			$criteria = $this->_getParam('criteria');
			if (!is_array($criteria))
				$criteria = json_decode(base64_decode($criteria), 1);
			
			$this->view->criteria = $criteria;
            
            // Special handling for TouchNet transaction.
            if (strlen($criteria['name']) == 6 && is_numeric($criteria['name']))
            {
                $touchnet = Touchnet::getRepository()->findOneBy(array('payment_order_id' => $criteria['name']));
                
                if ($touchnet instanceof Touchnet)
                {
                    $touchnet_user = $touchnet->user;
                    $this->redirectToRoute(array('module' => 'registration', 'controller' => 'view', 'action' => 'ledger', 'id' => $touchnet_user->id));
                    return;
                }
            }
            
            $user_query = $this->em->createQueryBuilder()
                ->select('u')
                ->from('\Entity\User', 'u');
            
            if ($criteria['master_report'])
            {
                $user_query->orderBy('u.fc_assigned_session', 'ASC')
                    ->orderBy('u.fc_assigned_camp', 'ASC')
                    ->orderBy('u.fc_assigned_cabin', 'ASC')
                    ->orderBy('u.lastname', 'ASC');
            }
            else
            {
                $user_query->orderBy('u.lastname', 'ASC');
            }
            
			if ($criteria['name'])
			{
                $user_query->andWhere('CONCAT(u.firstname, CONCAT(\' \', u.lastname)) LIKE :query OR u.uin LIKE :query OR u.username LIKE :query');
                $user_query->setParameter('query', '%'.$criteria['name'].'%');
			}

			// Multiple selection criteria.
			$checkboxes = array('fc_app_type', 'fc_assigned_session', 'fc_assigned_camp');
			foreach($checkboxes as $field_name)
			{
				if (!empty($criteria[$field_name]))
				{
					$user_query->andWhere($user_query->expr()->in('u.'.$field_name, ':'.$field_name));
                	$user_query->setParameter($field_name, $criteria[$field_name]);
				}
			}

			// Single yes/no selection criteria.
			$yesnomaybe = array('fc_received_payment', 'fc_received_papers', 'fc_assignment_override');
			foreach($yesnomaybe as $field_name)
			{
				if (!empty($criteria[$field_name]))
				{
					$value = (strcmp($criteria[$field_name], 'yes') == 0) ? 1 : 0;
	                $user_query->andWhere('u.'.$field_name.' = :'.$field_name);
	                $user_query->setParameter($field_name, $value);
				}
			}
			
			if ($this->_getParam('format') == "csv")
			{
				$results_raw = $user_query->getQuery()->getArrayResult();
				
				$fields = array(
					'uin'		=> 'UIN',
					'username'	=> 'NetID',
					'app_type_text' => 'Applicant Type',
					'firstname'	=> 'First Name',
					'lastname'	=> 'Last Name',
					'gender'	=> 'Gender',
					'email'		=> 'E-mail Address',
					'phone'		=> 'Phone Number',
                    'birthday'  => 'Birthday',
                    'age'       => 'Age',
                    'shirt_size' => 'Shirt Size',
                    'fc_received_papers' => 'Received Paperwork',
					'fc_received_payment' => 'Received Payment',
					'fc_session_preference' => 'Session Preferences',
                    'fc_assigned_session' => 'Assigned Session',
                    'fc_assigned_camp' => 'Assigned Camp',
                    'fc_assigned_cabin' => 'Assigned Cabin',
                    'fc_assigned_bus' => 'Assigned Bus',
					'parentname' => 'Parent Name',
					'parentphone' => 'Parent Phone',
					'parentemail' => 'Parent E-mail',
					'alt1name'	=> 'Alternate Contact 1 Name',
					'alt1phone' => 'Alternate Contact 1 Phone',
					'alt1email'	=> 'Alternate Contact 1 E-mail',
					'alt2name'	=> 'Alternate Contact 2 Name',
					'alt2phone' => 'Alternate Contact 2 Phone',
					'alt2email'	=> 'Alternate Contact 2 E-mail',
				);

				if (in_array(FC_TYPE_COUNSELOR, $criteria['fc_app_type']) || in_array(FC_TYPE_COUNSELOR2, $criteria['fc_app_type']))
				{
					$fields['fc_org_affiliation'] = 'Organization Affiliation';

					$fields['fc_counselor_application_application_type'] = 'Counselor or Crew';
					$fields['fc_counselor_application_camp_history'] = 'Camp History';
					$fields['fc_counselor_application_experience'] = 'Benefit of Experience';
					$fields['fc_counselor_application_inexperienced_motivates'] = 'Inexperienced: Motivation to Apply';
					$fields['fc_counselor_application_experienced_responsibilities'] = 'Experienced: Responsibilities and Goals';
				}
				
				$export_data = array(array_values($fields));
				$app_types = $this->config->fishcamp->applicant_types->toArray();
                
                $session_a = \Entity\Session::getRepository()->findOneByName('A');
                $session_a_time = $session_a->startdate;
				
				foreach($results_raw as $record)
				{
					$record['app_type_text'] = $app_types[$record['fc_app_type']];
					$record['fc_received_papers'] = ($record['fc_received_papers'] == 1) ? 'Y' : 'N';
					$record['fc_received_payment'] = ($record['fc_received_payment'] == 1) ? 'Y' : 'N';
                    
                    $user_birthday = $record['birthdate'];
                    $record['birthday'] = date('m/d/Y', $user_birthday);
                    $record['age'] = floor(($session_a_time - $user_birthday) / (86400 * 365));

                    // Unroll counselor application array.
                    if ($record['fc_counselor_application'])
                    {
                    	foreach((array)$record['fc_counselor_application'] as $field_name => $field_val)
                    		$record['fc_counselor_application_'.$field_name] = $field_val;
                    }
                    
					$export_row = array();
					foreach($fields as $field_key => $field_name)
					{
						$export_row[] = (string)$record[$field_key];
					}
					$export_data[] = $export_row;
				}
				
				\DF\Export::csv($export_data, TRUE);
				$this->doNotRender();
				return;
			}
			else
			{
				$page_num = ($this->_hasParam('page')) ? $this->_getParam('page') : 1;
				$paginator = new \DF\Paginator\Doctrine($user_query, $page_num, 25);
				
				$this->view->pager = $paginator;
			}
		}
		
	}
}
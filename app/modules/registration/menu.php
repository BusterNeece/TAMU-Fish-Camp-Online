<?php
return array(
    'default' => array(
		
		'register' => array(
			'label' => 'Register Now',
			'module' => 'registration',
			'controller' => 'index',
			'order' => 20,
		),
		
		'manage_registration' => array(
			'label' => 'Registration',
            'module' => 'registration',
			'controller' => 'manage',
			'permission' => 'manage registration',
            'order' => -20,
			
			'pages'		=> array(

				'registration_index' => array(
					'label' => 'Manage Registration',
					'module' => 'registration',
					'controller' => 'manage',
					'action' => 'index',
					'permission' => 'manage registration',
				),
                
				'registration_search'		=> array(
					'label'		=> 'Search for Registrants',
					'module'	=> 'registration',
					'controller' => 'search',
					'action' 	=> 'index',
					'permission' => 'manage registration',
					
					'pages'		=> array(
						'registration_view'		=> array(
							'module'		=> 'registration',
							'controller'	=> 'view',
							'action'		=> 'index',
						),
					),
				),
                
                'reports' => array(
                    'label'     => 'Reports',
                    'module'    => 'registration',
                    'controller' => 'reports',
                    'action'    => 'index',
                    'permission' => 'manage registration',
                    'pages'     => array(
                        
                        'special_accommodations_report' => array(
                            'label'     => 'Special Requirements Report',
                            'module'    => 'registration',
                            'controller' => 'reports',
                            'action'    => 'special',
                            'permission' => 'manage registration',
                        ),
                        
                    ),
                ),

                'registration_codes' => array(
					'label' => 'Temporary Access Codes',
					'module' => 'registration',
					'controller' => 'codes',
					'action' => 'index',
					'permission' => 'manage registration',

					'pages' => array(
						'codes_edit' => array(
							'module' => 'registration',
							'controller' => 'codes',
							'action' => 'edit',
						),
					),
				),
                
                'assignment' => array(
                    'label'     => 'Run Auto-Assignment',
                    'module'    => 'registration',
                    'controller' => 'manage',
                    'action'    => 'assignment',
                    'permission' => 'administer all',
                ),

                'lockassignment' => array(
                    'label'     => 'Lock Auto-Assignment Results',
                    'module'    => 'registration',
                    'controller' => 'manage',
                    'action'    => 'lockassignment',
                    'permission' => 'administer all',
                ),
                
			),
        ),
    ),
);
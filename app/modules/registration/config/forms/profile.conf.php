<?php
/**
 * Personal profile form
 */

$config = Zend_Registry::get('config'); 

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'legend'		=> 'My Profile Details',
		'method'		=> 'post',
		'groups'		=> array(
		
			'personal'	=> array(
				'legend'	=> 'Personal Information',
				'elements'	=> array(
				
					'firstname'		=> array('text', array(
						'label' => 'First Name',
						'required' => true,
						'class' => 'half-width',
					)),
		
					'lastname'		=> array('text', array(
						'label' => 'Last Name',
						'required' => true,
						'class' => 'half-width',
					)),
					
					'addr1'			=> array('text', array(
						'label' => 'Current Address',
						'required' => true,
						'class' => 'full-width',
						'description' => 'If you will be requesting a scholarship, please specify an address that you will be able to check over the summer.',
					)),
					'addrcity'		=> array('text', array(
						'label' => 'City',
						'required' => true,
						'class' => 'half-width',
					)),
					'addrstate'		=> array('select', array(
						'label' => 'State',
						'multiOptions' => array_merge(array('' => 'Choose a State'), $config->general->states->toArray()),
						'required' => true,
						'default' => 'TX',
					)),
					'addrzip'		=> array('text', array(
						'label' => 'Zip Code',
						'required' => true,
					)),
					
					'gender'		=> array('radio', array(
						'label' => 'Gender',
						'multiOptions' => array('M' => 'Male', 'F' => 'Female'),
						'required' => true,
					)),
		
					'phone'			=> array('text', array(
						'label' => 'Student Phone Number',
						'class' => 'half-width',
					)),
					
					'birthdate'		=> array('unixdate', array(
						'label' => 'Birthdate',
						'description' => 'Format: MM/DD/YYYY',
						'start_year' => 1990,
						'end_year' => 2005,
					)),

					'email'			=> array('text', array(
						'label' => 'TAMU E-mail Address',
						'validators' => array(
							'EmailAddress'
						),
						'required' => true,
						'class' => 'half-width',
					)),
					
					'email2'		=> array('text', array(
						'label' => 'Student Preferred E-mail Address',
						'validators' => array(
							'EmailAddress'
						),
						'required' => true,
						'class' => 'half-width',
						'description' => 'This can be the same address as the one listed above. Notifications regarding your registration will be sent to this address. Check it regularly between the date of your registration and the start of Fish Camp.',
					)),
					
					'shirt_size'	=> array('radio', array(
						'label' => 'Shirt Size',
                        'required' => TRUE,
						'multiOptions' => array(
							'S'	=> 'Small',
							'M'	=> 'Medium',
							'L'	=> 'Large',
							'XL' => 'X-Large',
							'2XL' => '2X-Large',
							'3XL' => '3X-Large',
						),
					)),
					
					'race'			=> array('select', array(
						'label' => 'How do you describe yourself?',
						'multiOptions' => array(
							'NA'	=> 'No Response',
							'AI'	=> 'American Indian',
							'AP'	=> 'Asian/Pacific Islander',
							'B'		=> 'Black/African American',
							'L'		=> 'Latin American/Other Hispanic',
							'M'		=> 'Mexican American/Chicano',
							'PR'	=> 'Puerto Rican',
							'W'		=> 'White',
							'O'		=> 'Other',
						),
					)),
					
					'college'		=> array('select', array(
						'label' => 'Your College',
						'multiOptions' => array(
							'NA'	=> 'No Response',
							'AG'	=> 'Agriculture and Life Sciences',
							'AR'	=> 'Architecture',
							'BA'	=> 'Business',
							'ED'	=> 'Education',
							'EN'	=> 'Engineering',
							'EX'	=> 'Other - Exchange Student',
							'GE'	=> 'Geoscience',
							'GS'	=> 'General Studies',
							'GV'	=> 'Other - Galveston Campus',
							'HC'	=> 'Rural and Public Health (Health Center)',
							'LA'	=> 'Liberal Arts',
							'SC'	=> 'Science',
							'VM'	=> 'Veterinary Medicine',
							'ZZ'	=> 'Other',
						),
					)),

					'fc_org_affiliation' => array('textarea', array(
						'label' => 'Organization Affiliations',
						'description' => 'Please list any and all other organizations you are currently a member of. Avoid using acronyms.',
						'class' => 'full-width half-height',
					)),
					
				),
			),
			
			'parent' => array(
				'legend'	=> 'Your Parent/Guardian',
				'elements'	=> array(
					
					'parentname' => array('text', array(
						'label' => 'Name',
						'required' => true,
						'class' => 'full-width',
					)),
					
					'parentphone' => array('text', array(
						'label' => 'Phone Number',
						'required' => true,
					)),
					
					'parentemail' => array('text', array(
						'label' => 'E-mail Address',
						'validators' => array(
							'EmailAddress'
						),
						'required' => true,
						'class' => 'half-width',
					)),
					
					'parentaddr' => array('textarea', array(
						'label' => 'Address',
						'class' => 'full-width half-height',
						'required' => true,
					)),
					
				),
			),
			
			'alt1' => array(
				'legend'	=> 'Alternate Contact #1',
				'elements'	=> array(
					
					'alt1name' => array('text', array(
						'label' => 'Name',
						'required' => true,
						'class' => 'full-width',
					)),
					
					'alt1phone' => array('text', array(
						'label' => 'Phone Number',
						'required' => true,
					)),
					
					'alt1email' => array('text', array(
						'label' => 'E-mail Address',
						'validators' => array(
							'EmailAddress'
						),
						'required' => true,
						'class' => 'half-width',
					)),
					
					'alt1addr' => array('textarea', array(
						'label' => 'Address',
						'class' => 'full-width half-height',
						'required' => true,
					)),
				
				),
			),
			
			'alt2' => array(
				'legend'	=> 'Alternate Contact #2',
				'elements'	=> array(
                    
					'alt2name' => array('text', array(
						'label' => 'Name',
						'required' => true,
						'class' => 'full-width',
					)),
					
					'alt2phone' => array('text', array(
						'label' => 'Phone Number',
						'required' => true,
					)),
					
					'alt2email' => array('text', array(
						'label' => 'E-mail Address',
						'validators' => array(
							'EmailAddress'
						),
						'required' => true,
						'class' => 'half-width',
					)),
					
					'alt2addr' => array('textarea', array(
						'label' => 'Address',
						'class' => 'full-width half-height',
						'required' => true,
					)),
				
				),
			),
			
			'medical' => array(
				'legend'	=> 'Medical Information',
				'description' => 'Notice: Although qualified medical personal are available at Fish Camp to provide certain medical care, they cannot administer some forms of aid without the consent of the person or his/her parents if he/she is a minor. The following must be completed.',
				
				'elements'	=> array(
					
					'insname' => array('text', array(
						'label' => 'Insurance Provider',
						'class' => 'full-width',
						'required' => true,
					)),
					'insgrouppolicy' => array('text', array(
						'label' => 'Group/Policy #',
						'class' => 'half-width',
						'required' => true,
					)),
					'inspolicyssn' => array('text', array(
						'label' => 'Supplemental Policy #',
						'class' => 'half-width',
					)),
					'inspolicyholder' => array('text', array(
						'label' => 'Policy Holder Name',
						'class' => 'half-width',
					)),
					'tetanus' => array('unixdate', array(
						'label' => 'Date of Last Tetanus (Td/Tdap) Booster',
						'required' => true,
						'start_year' => 1990,
						'end_year' => date('Y'),
					)),
					'allergies' => array('textarea', array(
						'label' => 'Allergies',
						'class' => 'half-width half-height',
                        'description' => '<b>If none,</b> leave this field blank.',
					)),
					'medications' => array('textarea', array(
						'label' => 'Medications Being Taken',
						'class' => 'half-width half-height',
                        'description' => '<b>If none,</b> leave this field blank.',
					)),
					'othermedical' => array('textarea', array(
						'label' => 'Other Medical Concerns',
						'class' => 'half-width half-height',
                        'description' => '<b>If none,</b> leave this field blank.',
					)),
					'specialaccommodations' => array('textarea', array(
						'label' => 'Special Accommodations',
						'class' => 'half-width half-height',
                        'description' => '<b>If none,</b> leave this field blank.',
					)),
					
				),
			),

			'submit'	=> array(
				'elements'	=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save Changes',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);
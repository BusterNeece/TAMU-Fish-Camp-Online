<?php
/**
 * Personal profile form
 */

$config = \Zend_Registry::get('config');
$fc = \Zend_Registry::get('fc');

// List of sessions.
$session_select = \Entity\Session::fetchSelect(TRUE);
$session_select['WL'] = 'Waitlist';

// List of camps.
$camp_select = \Entity\Camp::fetchSelect(TRUE);
$camp_select['WL'] = 'Waitlist';

// List of cabins.
$cabin_select = \Entity\Cabin::fetchSelect(TRUE);
$cabin_select['WL'] = 'Waitlist';

// List of buses.
$bus_select = array(NULL => 'None');
$freshmen_per_bus = $config->fishcamp->freshmen_per_bus;
for($i = 1; $i <= 22; $i++)
{
	$bus_name = $fc->getBusNameByNumber($i);
	$bus_select[$bus_name] = $bus_name;
}
$bus_select['WL'] = 'Waitlist';

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'legend'		=> 'Administrative Registration Updates',
		'method'		=> 'post',
		'groups'		=> array(
			
			'admin'		=> array(
				'legend'	=> 'Administrative Registration Updates',
				'elements'	=> array(
				
					'fc_app_type' => array('select', array(
						'label' => 'Applicant Type',
						'multiOptions' => $config->fishcamp->applicant_types->toArray(),
					)),
					
					'fc_assigned_session' => array('select', array(
						'label' => 'Assigned Session',
						'multiOptions' => $session_select,
					)),
					
					'fc_assigned_camp' => array('select', array(
						'label' => 'Assigned Camp',
						'multiOptions' => $camp_select,
					)),
					
					'fc_assigned_cabin' => array('select', array(
						'label' => 'Assigned Cabin',
						'multiOptions' => $cabin_select,
					)),
					
					'fc_assigned_bus' => array('select', array(
						'label' => 'Assigned Bus',
						'multiOptions' => $bus_select,
					)),

					'fc_received_papers' => array('radio', array(
						'label' => 'Received Paperwork',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),
					'fc_received_papers' => array('radio', array(
						'label' => 'Received Paperwork',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),
					'fc_received_payment' => array('radio', array(
						'label' => 'Received Payment',
						'description' => 'For counselors and co-chairs only; Freshmen will automatically be set by the ledger system.',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),

					'fc_assignment_override' => array('radio', array(
                        'label' => 'Lock Assignments',
                        'description' => 'If this field is set to "Yes", the user\'s assignment will not be modified by the auto-assignment process.',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                    )),
                    
					'fc_is_cancelled' => array('radio', array(
						'label' => 'Is Cancelled',
						'description' => 'If this flag is set to "Yes", this registrant will automatically be removed from any session, camp, cabin, or bus and will not be included in future assignment.',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),

					'specialaccommodation_notes' => array('textarea', array(
						'label' => 'Special Accommodation Notes',
						'class' => 'full-width half-height',
					)),

				),
			),
			
			'submit'	=> array(
				'elements'	=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save Changes',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);
<?php
/**
 * Post Ledger Item Form
 */

$config = Zend_Registry::get('config');
 
return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'ledger_item_type_id' => array('select', array(
                'label' => 'Transaction Type',
                'multiOptions' => \Entity\LedgerItemType::fetchSelect(TRUE),
                'required' => true,
            )),
            
            'amount' => array('text', array(
                'label' => 'Transaction Amount',
                'required' => true,
            )),
			
			'payment_method' => array('radio', array(
				'label' => 'Payment Method',
				'multiOptions' => $config->fishcamp->payment_methods->toArray(),
				'required' => true,
			)),
            
            /*
			'funding_source' => array('text', array(
				'label' => 'Funding Source (Scholarships Only)',
				'class' => 'half-width',
				'description' => 'Indicate the account used to supply scholarship funds.',
			)),
            */
            
            'ipayments_reference_number' => array('text', array(
                'label' => 'I-Payments Reference Number (Cash/Check Payment)',
            )),
			
			'check_number' => array('text', array(
				'label' => 'Check Number (Checks Only)',
			)),
			
			'check_date' => array('unixdate', array(
				'label' => 'Check Date (Checks Only)',
			)),
			
			'post_date' => array('unixdate', array(
				'label' => 'iPayments/Touchnet Post Date',
			)),
			
			'is_posted_to_famis' => array('radio', array(
				'label'	=> 'Is Posted to Famis',
				'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				'required' => true,
			)),
			
			'notes'		=> array('textarea', array(
				'label' => 'Notes',
				'class' => 'full-width full-height',
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);
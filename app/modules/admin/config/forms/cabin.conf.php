<?php
$sessions = \Entity\Session::fetchSelect();

$session_fields = array();
foreach($sessions as $session_name)
{
	$session_fields['cabin_gender_'.$session_name] = array('radio', array(
		'label' => 'Gender for Session '.$session_name,
		'multiOptions' => array(
			'M' => 'Male',
			'F' => 'Female',
			'X' => 'Inactive',
		),
	));
}

return array(
	'form' => array(
		'method'		=> 'post',
		'groups' 		=> array(

			'cabin_info' => array(
				'legend' => 'Cabin Details',
				'elements' => array(

					'name' => array('text', array(
						'label' => 'Cabin Name',
						'class' => 'full-width',
						'required' => true,
			        )),
			        'camp_id' => array('radio', array(
						'label' => 'Parent Camp',
						'multiOptions' => \Entity\Camp::fetchSelect(),
						'required' => true,
			        )),
			        'population' => array('text', array(
			        	'label' => 'Freshman Capacity',
			        	'required' => true,
			        )),
				),
			),

			'session_fields' => array(
				'legend' => 'Session-Specific Settings',
				'elements' => $session_fields,
			),

			'submit_grp' => array(
				'elements' => array(
					'submit' => array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save Changes',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),

		),
	),
);
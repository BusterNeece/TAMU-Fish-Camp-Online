<?php
use \Entity\User;
use \Entity\Role;

class Admin_UsersController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::getInstance()->isAllowed('admin_all');
    }

    public function indexAction()
    {
        if (isset($_GET['q']))
			$this->redirectFromHere(array('q' => $_GET['q']));
		
        $qb = $this->em->createQueryBuilder();
        
        $qb
            ->select('u')
            ->from('Entity\User', 'u')
            ->groupBy('u.id')
            ->orderBy('u.id', 'ASC');
        
		if ($this->_hasParam('q'))
		{
            $this->view->query = $this->_getParam('q');
            $q = '%'.$this->_getParam('q').'%';
            
            $qb->where('(u.lastname LIKE :q) OR (CONCAT(u.firstname, CONCAT(\' \', u.lastname)) LIKE :q) OR (u.uin LIKE :q) OR (u.username LIKE :q)');
            $qb->setParameter('q', $q);
		}
        
		$paginator = new \DF\Paginator\Doctrine($qb);
        $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);
		$this->view->pager = $paginator;
    }

    public function addAction()
    {
		$form = new \DF\Form($this->current_module_config->forms->user_new->form);

        if( !empty($_POST) && $form->isValid($_POST) )
        {
			$data = $form->getValues();
			$uins_raw = explode("\n", $data['uin']);
			
			foreach((array)$uins_raw as $uin)
			{
				$uin = trim($uin);
				
				$user = User::getOrCreate($uin, FALSE);
				$user->fromArray(array('roles' => $data['roles']));
				$this->em->persist($user);
				
				$this->alert('User <a href="'.\DF\Url::route(array('module' => 'admin', 'controller' => 'users', 'action' => 'edit', 'id' => $user->id)).'" title="Edit User">'.$user->lastname.', '.$user->firstname.'</a> successfully updated/added.');
			}

			$this->em->flush();
			
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
        }
		
        $this->view->form = $form;
    }
    
    public function editAction()
    {
		// Handle UIN translation.
		if ($this->_hasParam('uin'))
		{
			$user = User::getRepository()->findOneBy(array('uin' => $this->_getParam('uin')));
			
			if ($user)
				$this->redirectFromHere(array('uin' => NULL, 'id' => $user->id));
			else
				throw new \DF\Exception\DisplayOnly('User not found!');
			
			return;
		}
		
        $id = $this->getRequest()->getParam('id');
        $user = User::find($id);
		
		$form = new \DF\Form($this->current_module_config->forms->user_edit->form);
		
		if ($user instanceof User)
			$form->setDefaults($user->toArray(TRUE, TRUE));
	
		if( !empty($_POST) && $form->isValid($_POST) )
		{
			try
			{
				$data = $form->getValues();

				$user->fromArray($data);
                $user->save();
                
				$this->flash('User updated!', 'green');
			}
			catch( \Exception $e )
			{
				$form->setErrors(array(
					$e->getMessage()
				));
				$this->flash($e->getMessage());
			}
			
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
			return;
		}

        $this->view->form = $form;
    }
}
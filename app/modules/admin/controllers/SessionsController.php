<?php
use \Entity\Session;

class Admin_SessionsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::isAllowed('manage registration lookups');
    }
    
    public function indexAction()
    {
		$this->view->sessions = Session::fetchArray('name');
    }
	
	public function editAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->session->form);
		
		if ($this->_hasParam('id'))
		{
			$id = (int)$this->_getParam('id');
			$record = Session::find($id);

			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Session))
				$record = new Session;
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Record updated.', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Session');
        $this->renderForm($form);
	}
	
	public function deleteAction()
	{
		$record = Session::find($this->_getParam('id'));
		if ($record instanceof Session)
			$record->delete();
			
		$this->alert('Record deleted.', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}
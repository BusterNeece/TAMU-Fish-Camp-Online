<?php
class Admin_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('administer all');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
	{
        $this->redirectHome();
        return;
    }
}
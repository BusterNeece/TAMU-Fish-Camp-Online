<?php
return array(
    'default' => array(
        'accounting' => array(
			'label' => 'Accounting',
            'module' => 'accounting',
			'controller' => 'index',
			'permission' => 'view accounting',
            'order' => -30,
			
			'pages'		=> array(

				'acct_index' => array(
					'label' => 'Manage Accounting',
					'module' => 'accounting',
					'controller' => 'index',
					'action' => 'index',
				),
				
				'reports'		=> array(
					'label'	=> 'Transaction Report',
					'module' => 'accounting',
					'controller' => 'reports',
					'action' => 'index',
					'permission' => 'view accounting',
				),
				'reconciliation' => array(
					'label' => 'Cumulative Finance Overview',
					'module' => 'accounting',
					'controller' => 'reconciliation',
					'permission' => 'view accounting',
				),
				
			),
        ),
    ),
);
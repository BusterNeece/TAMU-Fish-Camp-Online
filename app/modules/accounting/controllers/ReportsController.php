<?php
use \Entity\Ledger;
use \Entity\LedgerItemType;
use \Entity\LedgerItemTypeAmount;

class Accounting_ReportsController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('view accounting');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
		// Convert form submission into URL parameter.
		if (isset($_REQUEST['criteria']))
		{
			$criteria = $_REQUEST['criteria'];
			$criteria['start_date'] = \DF\Form\Element\UnixDate::processArray($criteria['start_date']);
			$criteria['end_date'] = \DF\Form\Element\UnixDate::processArray($criteria['end_date']);

			$this->redirectFromHere(array(
				'criteria' => base64_encode(\Zend_Json::encode($criteria))
			));
			return;
		}
		
		$ledger_types = LedgerItemType::fetchSelect(FALSE, FALSE);
		$this->view->ledger_types = $ledger_types;
		
		$payment_methods = $this->config->fishcamp->payment_methods->toArray();
		$this->view->payment_methods = $payment_methods;
		
		if ($this->_hasParam('criteria'))
		{
			$criteria = $this->_getParam('criteria');
			if (!is_array($criteria))
				$criteria = json_decode(base64_decode($criteria), 1);
			
			$this->view->criteria = $criteria;
            
            $query = $this->em->createQueryBuilder()
                ->select('l, lit, u, t')
                ->from('\Entity\Ledger', 'l')
                ->leftJoin('l.type', 'lit')
                ->leftJoin('l.user', 'u')
                ->leftJoin('l.touchnet', 't')
                ->orderBy('l.post_date', 'ASC');
			
			if ($criteria['ledger_item_type_id'])
			{
                $query->andWhere($query->expr()->in('l.ledger_item_type_id', ':item_types'));
                $query->setParameter('item_types', $criteria['ledger_item_type_id']);
			}
			if ($criteria['payment_method'])
			{
                $query->andWhere($query->expr()->in('l.payment_method', ':payment_methods'));
                $query->setParameter('payment_methods', $criteria['payment_method']);
			}
			if ($criteria['start_date'] || $criteria['end_date'])
			{
				$start_date = ($criteria['start_date']) ? $criteria['start_date'] : 0;

				$end_date = ($criteria['end_date']) ? $criteria['end_date'] : time();
				$end_date = strtotime(date('m/d/Y', $end_date).' 23:59:59');
                
				$query->andWhere('l.post_date >= :start_date AND l.post_date <= :end_date');
                $query->setParameters(array('start_date' => $start_date, 'end_date' => $end_date));
			}
			
			if ($this->_getParam('format') == "csv")
			{
				$results_raw = $query->getQuery()->getArrayResult();
				
				$export_data = array(
					array(
						'ID',
                        'TouchNet ID',
						'Amount',
						'Creation Date',
						'FAMIS Post Date',
						'User First Name',
						'User Last Name',
						'User UIN',
						'Type',
						'Payment Method',
						'Submitted By',
						'Notes',
					)
				);
				
				foreach($results_raw as $item)
				{
					$export_data[] = array(
						$item['id'],
                        ($item['touchnet']) ? $item['touchnet']['payment_order_id'] : 'N/A',
						number_format($item['amount'], 2),
						$item['created_at']->format('m/d/Y g:ia'),
						date('m/d/Y g:ia', $item['post_date']),
						$item['user']['firstname'],
						$item['user']['lastname'],
						$item['user']['uin'],
						$item['type']['name'],
						$item['payment_method'],
						$item['submitter'],
						$item['notes'],
					);
				}
				
				\DF\Export::csv($export_data, TRUE);
				$this->doNotRender();
				return;
			}
			else
			{
				$paginator = new \DF\Paginator\Doctrine($query);
				$paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);
				$this->view->pager = $paginator;
			}
		}
	}
}
<?php
class Accounting_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('view accounting');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {}
}
<?php
return array(
    'default' => array(
        
        'view_events' => array(
            'label' => 'Events',
            'module' => 'events',
            'show_only_with_subpages' => TRUE,
            
            'pages' => array(

                'index' => array(
                    'label' => 'Events List',
                    'module' => 'events',
                    'controller' => 'index',
                    'action' => 'index',
                    'permission' => 'is counselor',
                ),
                
                'calendar' => array(
                    'module' => 'events',
                    'controller' => 'index',
                    'action' => 'calendar',
                ),

                'registered' => array(
                    'label' => 'My Registered Events',
                    'module' => 'events',
                    'controller' => 'index',
                    'action' => 'registered',
                    'permission' => 'is counselor',
                ),

                'manage_events' => array(
                    'label' => 'Manage Events',
                    'module' => 'events',
                    'controller' => 'manage',
                    'action' => 'index',
                    'permission' => 'manage events',
                    
                    'pages' => array(
                        
                        'category' => array(
                            'module' => 'events',
                            'controller' => 'manage',
                            'action' => 'category',
                        ),
                        'edit' => array(
                            'module' => 'events',
                            'controller' => 'manage',
                            'action' => 'edit',
                        ),
                        'attendees' => array(
                            'module' => 'events',
                            'controller' => 'manage',
                            'action' => 'attendees',
                        ),
                        
                    ),
                ),
                
            ),
        ),
    ),
);
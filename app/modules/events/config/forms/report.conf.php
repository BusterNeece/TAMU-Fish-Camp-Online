<?php
return array(
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			
            'view' => array('select', array(
                'label' => 'Event Type',
                'required' => true,
                'multiOptions' => \Entity\EventType::fetchSelect(),
            )),
            
            'start' => array('unixdate', array(
                'label' => 'Start Date',
                'required' => true,
            )),
            
            'end' => array('unixdate', array(
                'label' => 'End Date',
                'required' => true,
            )),
			
			'submit' => array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Generate Report',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);
<?php
/**
 * Add/Edit Event Type Form
 */

$module_config = \Zend_Registry::get('module_config');

return array(
	'form' => array(
		'method'		=> 'post',
        
		'elements'		=> array(

			'id' => array('text', array(
				'label' => 'Event Type Programmatic Code',
				'description' => 'This code cannot be changed once set. Codes can only use the following characters: A-Z a-z 0-9 - _',
				'class' => 'half-width',
				'required' => true,
			)),
			
            'name' => array('text', array(
				'label' => 'Event Type Name',
				'class'	=> 'full-width',
                'required' => true,
	        )),
            
            'is_hidden' => array('select', array(
                'label' => 'Hidden From Public Directories',
                'required' => true,
                'multiOptions' => array(0 => 'No', 1 => 'Yes'),
            )),
            
            'confirmation_message' => array('textarea', array(
				'label' => 'Confirmation Message (Optional)',
				'description' => 'Enter the body of the e-mail message that the attendee will receive once they are confirmed.',
				'class' => 'full-width full-height',
            )),
			
			'submit' => array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);
<?php
return array(
    'default' => array(

        'manage_registration' => array(
            'pages' => array(

                'checkin' => array(
                    'label' => 'Check In',
                    'module' => 'checkin',
                    'controller' => 'index',
                    'action' => 'index',
                    'permission' => 'manage registration',
                    'order' => 10,
                    
                    'pages' => array(
                        
                        'checkin_status' => array(
                            'label' => 'Session Status',
                            'module' => 'checkin',
                            'controller' => 'status',
                            'action' => 'index',
                            'permission' => 'manage registration',
                        ),
                        
                    ),
                ),

            ),
        ),
        
    ),
);
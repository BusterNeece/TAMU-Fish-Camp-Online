<?php
class Checkin_StatusController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage registration');
    }
    
    public function indexAction()
    {
        $fc = \Zend_Registry::get('fc');
        $fc_settings = $fc->getSettings();
        
        $closest_session = 'A';
        $closest_session_time = 0;
        
        foreach($fc_settings['sessions'] as $session_name => $session)
        {
            $session_date = $session['start_date'];
            $time_diff = abs($session_date - time());
            
            if ($time_diff < $closest_session_time || $closest_session_time == 0)
            {
                $closest_session = $session_name;
                $closest_session_time = $time_diff;	
            }
        }
        
        $session_name = $this->_getParam('session', $closest_session);
        
        // Gather information about the current session.
        $session_info = $fc_settings['sessions'][$session_name];
        $current_population = $session_info['sess_num_current'];
        
        $live_data_raw = $this->em->createQuery('SELECT c FROM \Entity\Checkin c WHERE c.created_at BETWEEN :start_time AND :end_time ORDER BY c.created_at DESC')
            ->setParameter('start_time', date('Y-m-d', $session_info['startdate']))
            ->setParameter('end_time', date('Y-m-d', $session_info['enddate'] - 86400))
            ->getArrayResult();
        
        $data_by_location = array();
        $users_checked_in = array();
        
        foreach($live_data_raw as $live_data_row)
        {
            $user_id = $live_data_row['user_id'];
            
            $data_by_location[$live_data_row['origin']][$user_id] = $user_id;
            $users_checked_in[$user_id] = $user_id;
        }
        
        $percent_checked_in = round((count($users_checked_in) / $current_population) * 100);
        
        // Compose list of no-shows.
        $no_show_campers = array();
        $no_show_campers_raw = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.fc_app_type = :fc_app_type AND u.fc_received_payment = 1 AND u.fc_received_papers = 1 AND u.fc_app_completed != 0 AND u.fc_assigned_session = :session_name AND u.id NOT IN (:user_ids) ORDER BY u.fc_assigned_camp ASC, u.lastname ASC')
            ->setParameters(array(
                'fc_app_type'       => FC_TYPE_FRESHMAN,
                'session_name'      => $session_name,
                'user_ids'          => array(0 => 0) + $users_checked_in,
            ))
            ->getArrayResult();
        
        foreach($no_show_campers_raw as $camper)
        {
            $no_show_campers[$camper['id']] = $camper;
        }
        
        
        // Excel export functionality.
        if ($this->_getParam('format', '') == "csv")
        {
            $export_data = array(
                array('No-Shows: Session '.$session_name),
                array(
                    'First Name',
                    'Last Name',
                    'UIN',
                    'Session',
                    'Camp',
                    'Cabin',
                    'Bus',
                )
            );
            
            foreach($no_show_campers_raw as $camper)
            {
                $export_data[] = array(
                    $camper['firstname'],
                    $camper['lastname'],
                    $camper['uin'],
                    $camper['fc_assigned_session'],
                    $camper['fc_assigned_camp'],
                    $camper['fc_assigned_cabin'],
                    $camper['fc_assigned_bus'],
                );
            }
            
            \DF\Export::csv($export_data);
            return;
        }
        
        $this->view->assign(array(
            'sessions'			=> array_keys($fc_settings['sessions']),
            'session_name'		=> $session_name,
            'data_by_location'  => $data_by_location,
            'percent_checked_in' => $percent_checked_in,
            'no_show_campers'	=> $no_show_campers,
            'no_show_campers_num'	=> count($no_show_campers),
        ));
        
        if (!$this->isMobile())
        {
            $tabs_path = $this->getViewScript('index_tabs');
            $this->view->layout()->tabs = $this->view->render($tabs_path);
        }
    }
}
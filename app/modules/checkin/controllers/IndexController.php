<?php
use \Entity\User;
use \Entity\Checkin;

class Checkin_IndexController extends \DF\Controller\Action
{
    protected $_auth_session;
    protected $_auth_code = 'fc2011!';
    
    public function init()
    {
        $this->_auth_session = new \Zend_Session_Namespace('fco_checkin_auth');
        parent::init();
    }
    
    public function permissions()
    {
        if (\DF\Acl::isAllowed('manage registration'))
        {
            return true;
        }
        else if ($this->_auth_session->is_logged_in)
        {
            return true;
        }
        else
        {
            $current_page = $this->_getParam('action');
            
            if ($current_page != "login")
            {
                $this->redirectFromHere(array('action' => 'login'));
                return;
            }
            else
            {
                return true;
            }
        }
    }
	
    public function indexAction()
    {
        if ($this->_hasParam('q'))
        {
            $q = $this->_getParam('q');
            
            $result = \FCO\CheckInManager::checkInUser($q, 'desktop');
            $this->view->result = $result;

            $this->render('index_result');
            return;
        }
    }
    
    public function loginAction()
    {
        if ($this->_hasParam('q'))
        {
            $q = $this->_getParam('q', '');
            
            if (strcmp($q, $this->_auth_code) == 0)
            {
                $this->_auth_session->is_logged_in = TRUE;
                
                $this->redirectFromHere(array('action' => 'index'));
                return;
            }
        }
    }
}
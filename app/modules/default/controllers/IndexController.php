<?php
class IndexController extends \DF\Controller\Action
{
	/**
	 * Main display.
	 */
    public function indexAction()
    {   
        $this->view->sessions = \Entity\Session::fetchAll();
	}
}
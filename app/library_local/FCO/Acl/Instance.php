<?php
/**
 * FishCampOnline Access Control - DF\Acl Override
 */
namespace FCO\Acl;

use \Entity\User;

class Instance extends \DF\Acl\Instance
{
	public function userAllowed($action, User $user = null)
    {
		if (parent::userAllowed($action, $user))
    		return true;
    	
    	// All subsequent permissions require logged in user.
    	if (!($user instanceof User))
    		return false;
    	
		$action = array_map('strtolower', (array)$action);
        asort($action);
        
        foreach($action as $action_name)
        {
			if ($this->_userAllowed($action_name, $user))
				return true;
        }
        
        return false;
    }
    
    protected function _userAllowed($action, User $user = null)
    {
    	// Permissions cascade to the next highest registration level.
    	switch($action)
    	{
    		case "is freshman":
    			if ($user->fc_app_type == FC_TYPE_FRESHMAN)
    				return true;
    			
    		case "is counselor":
    			if ($user->fc_app_type == FC_TYPE_COUNSELOR2)
    				return true;
    		
    		case "is chair":
    			if ($user->fc_app_type == FC_TYPE_CHAIR)
    				return true;
    		
    		case "is director":
    			if ($user->fc_app_type == FC_TYPE_DIRECTOR)
    				return true;
    			
    		default:
    			return false;
    		break;
    	}
    }
}
<?php
namespace Entity;

use \Entity\Event;
use \Entity\EventType;
use \Entity\User;
use \Entity\Training;

/**
 * EventAttendee
 *
 * @Table(name="event_attendees")
 * @Entity
 * @HasLifecycleCallbacks
 */
class EventAttendee extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = new \DateTime('NOW');
        $this->updated_at = new \DateTime('NOW');
    }

    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime('NOW');
    }

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="event_id", type="integer") */
    protected $event_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="confirmed", type="integer", length=1, nullable=true) */
    protected $confirmed;

    /** @Column(name="is_processed", type="integer", length=1, nullable=true) */
    protected $is_processed;

    /** @Column(name="created_at", type="datetime", nullable=true) */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime", nullable=true) */
    protected $updated_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\Event", inversedBy="attendees")
     * @JoinColumn(name="event_id", referencedColumnName="id")
     */
    protected $event;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * Static Functions
     */
    
    public static function addAttendee($event, $user, $confirmed)
    {
        $ea = self::getRepository()->findOneBy(array(
            'event_id' => $event['id'],
            'user_id' => $user['id']
        ));
        
        if (!($ea instanceof self))
        {
            $ea = new self;
            $ea->user = $user;
            $ea->event = $event;
        }
        
        $ea->confirmed = ($confirmed) ? 1 : 0;
        
        $event_type = $event['type'];

        $message = $event_type->confirmation_message;
        if (!empty($message) && $ea->confirmed == 1)
        {
            \DF\Messenger::send(array(
                'to'        => $user['email'],
                'subject'   => 'Event Attendance Confirmed',
                'module'    => 'events',
                'template'  => 'confirmation',
                'vars'      => array(
                    'message' => $message,
                    'event' => $event->type->name,
                ),
            ));
        }
        
        $ea->save();
    }
    
    public static function deleteAttendee($event, $user)
    {
        $ea = self::getRepository()->findOneBy(array(
            'event_id' => $event['id'],
            'user_id' => $user['id']
        ));
        
        if ($ea instanceof self)
        {
            $ea->delete();
        }
    }
}
<?php

namespace Entity;

/**
 * Entity\Cabin
 *
 * @Table(name="cabin")
 * @Entity
 */
class Cabin extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="camp_id", type="integer") */
    protected $camp_id;

    /** @Column(name="population", type="integer") */
    protected $population;
    
    /** @Column(name="cabin_gender_A", type="string", length=2, nullable=true) */
    protected $cabin_gender_A;
    
    /** @Column(name="cabin_gender_B", type="string", length=2, nullable=true) */
    protected $cabin_gender_B;
    
    /** @Column(name="cabin_gender_C", type="string", length=2, nullable=true) */
    protected $cabin_gender_C;
    
    /** @Column(name="cabin_gender_D", type="string", length=2, nullable=true) */
    protected $cabin_gender_D;
    
    /** @Column(name="cabin_gender_E", type="string", length=2, nullable=true) */
    protected $cabin_gender_E;
    
    /** @Column(name="cabin_gender_F", type="string", length=2, nullable=true) */
    protected $cabin_gender_F;
    
    /** @Column(name="cabin_gender_G", type="string", length=2, nullable=true) */
    protected $cabin_gender_G;
    
    /**
     * @ManyToOne(targetEntity="Entity\Camp")
     * @JoinColumn(name="camp_id", referencedColumnName="id")
     */
    protected $camp;

    public static function fetchSelect($add_blank = FALSE)
    {
        $select = array();
        if ($add_blank)
            $select[''] = 'Select...';

        $em = self::getEntityManager();
        $cabins_raw = $em->createQuery('SELECT cn, cm FROM Entity\Cabin cn JOIN cn.camp cm ORDER BY cm.name ASC, cn.name ASC')->getArrayResult();

        foreach($cabins_raw as $cabin)
            $select[$cabin['name']] = $cabin['camp']['name'].': '.$cabin['name'];

        return $select;
    }
}
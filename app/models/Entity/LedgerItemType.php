<?php

namespace Entity;

/**
 * Entity\LedgerItemType
 *
 * @Table(name="ledger_item_type")
 * @Entity
 */
class LedgerItemType extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="category", type="string", length=255, nullable=true) */
    protected $category;

    /** @Column(name="budget", type="decimal", nullable=true) */
    protected $budget;
    
    /** @OneToMany(targetEntity="Entity\LedgerItemTypeAmount", mappedBy="ledger_item_type") */
    protected $amounts;
    
    /**
     * Static Functions
     */

    public static function fetchSelect($add_blank = FALSE, $use_groups = TRUE)
	{
		$config = \Zend_Registry::get('config');
		$ledger_categories = $config->fishcamp->ledger_categories->toArray();
		
		$all_types = self::fetchAll();
		$ledger_select = array();

		if ($add_blank !== FALSE)
			$ledger_select[''] = ($add_blank !== TRUE) ? $add_blank : 'Select...';
		
		foreach($ledger_categories as $category_name)
		{
			$ledger_select[$category_name] = array();
		}
		
		foreach($all_types as $type)
		{
			$category = $type['category'];
			$ledger_select[$category][$type['id']] = $type['name'];
		}
		
		if (!$use_groups)
		{
			$ledger_select_new = array();
			foreach($ledger_select as $category => $items)
			{
				foreach($items as $item_id => $item_name)
				{
					$ledger_select_new[$item_id] = $category.': '.$item_name;
				}
			}
			$ledger_select = $ledger_select_new;
		}
		
		return $ledger_select;
	}
}
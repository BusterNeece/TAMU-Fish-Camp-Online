<?php
namespace Entity;

/**
 * @Table(name="session")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Session extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /** @Column(name="name", type="string", length=10, nullable=true) */
    protected $name;
    
    /** @Column(name="startdate", type="integer") */
    protected $startdate;
    
    /** @Column(name="enddate", type="integer") */
    protected $enddate;

    /** @Column(name="status", type="smallint", nullable=true) */
    protected $status;
    
    /** @Column(name="num_current", type="smallint", nullable=true) */
    protected $num_current;
    
    /** @Column(name="num_current_m", type="smallint", nullable=true) */
    protected $num_current_m;
    
    /** @Column(name="num_current_f", type="smallint", nullable=true) */
    protected $num_current_f;
    
    /** @Column(name="num_waitlist", type="smallint", nullable=true) */
    protected $num_waitlist;
    
    /** @Column(name="num_waitlist_m", type="smallint", nullable=true) */
    protected $num_waitlist_m;
    
    /** @Column(name="num_waitlist_f", type="smallint", nullable=true) */
    protected $num_waitlist_f;
    
    /** @Column(name="num_full", type="smallint", nullable=true) */
    protected $num_full;
    
    /** @Column(name="num_full_m", type="smallint", nullable=true) */
    protected $num_full_m;
    
    /** @Column(name="num_full_f", type="smallint", nullable=true) */
    protected $num_full_f;
    
    /** @Column(name="num_total", type="smallint", nullable=true) */
    protected $num_total;
    
    /** @Column(name="num_total_m", type="smallint", nullable=true) */
    protected $num_total_m;
    
    /** @Column(name="num_total_f", type="smallint", nullable=true) */
    protected $num_total_f;

    /** @PreUpdate */
    public function updateSessionStatus()
    {
        if (\FCO\Registration::getStatus(FC_TYPE_FRESHMAN) == FC_STATUS_OPEN)
        {
            $is_waitlisted = ($this->num_current_m > $this->num_waitlist_m && $this->num_current_f > $this->num_waitlist_f);
            $is_full = ($this->num_current_m > $this->num_full_m && $this->num_current_f > $this->num_full_f);

            if (!$is_waitlisted)
                $this->status = FC_STATUS_OPEN;
            elseif (!$is_full)
                $this->status = FC_STATUS_WAITLIST;
            else
                $this->status = FC_STATUS_FULL;
        }
        else
        {
            $this->status = FC_STATUS_CLOSED;
        }
    }

    /**
     * Static Functions
     */
    
    public static function fetchAll()
    {
        $em = \Zend_Registry::get('em');
        $sessions_raw = self::fetchArray('name');
        $sessions = array();

        foreach($sessions_raw as $session)
        {
            $session['date'] = date('F j', $session['startdate']).' - '.date('F j', $session['enddate']);
            $session['status_text'] = self::getStatusName($session['status']);

            $sessions[$session['name']] = $session;
        }

        return $sessions;
    }

    public static function getStatusName($status)
    {
        $config = \Zend_Registry::get('config');
        return $config->fishcamp->status_codes->{$status};
    }

    public static function fetchSelect($add_blank = FALSE)
    {
        $select = array();
        if ($add_blank)
            $select[''] = 'Select...';

        $sessions = self::fetchAll();
        foreach($sessions as $session)
            $select[$session['name']] = 'Session '.$session['name'].': '.$session['date'];

        return $select;
    }
}
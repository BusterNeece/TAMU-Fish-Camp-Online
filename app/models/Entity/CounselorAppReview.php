<?php

namespace Entity;

/**
 * @Table(name="counselor_app_review")
 * @Entity
 */
class CounselorAppReview extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->is_read = FALSE;
    }

    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="camp_id", type="integer") */
    protected $camp_id;

    /** @Column(name="session_id", type="integer") */
    protected $session_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="is_read", type="boolean", nullable=true) */
    protected $is_read;

    /**
     * @ManyToOne(targetEntity="Camp")
     * @JoinColumn(name="camp_id", referencedColumnName="id")
     */
    protected $camp;

    /**
     * @ManyToOne(targetEntity="Session")
     * @JoinColumn(name="session_id", referencedColumnName="id")
     */
    protected $session;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
}
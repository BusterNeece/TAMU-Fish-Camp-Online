<?php

namespace Entity;

/**
 * Entity\LedgerItemTypeAmount
 *
 * @Table(name="ledger_item_type_amount")
 * @Entity
 */
class LedgerItemTypeAmount extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="ledger_item_type_id", type="integer") */
    protected $ledger_item_type_id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="amount", type="decimal", scale=2, nullable=true) */
    protected $amount;

    /** @ManyToOne(targetEntity="Entity\LedgerItemType", inversedBy="amounts") */
    protected $ledger_item_type;

    public function __construct()
    {
        $this->LedgerItemType = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
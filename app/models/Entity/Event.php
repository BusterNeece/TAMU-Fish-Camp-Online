<?php
namespace Entity;


/**
 * Event
 *
 * @Table(name="events")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Event extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->attendees = new \Doctrine\Common\Collections\ArrayCollection();
        $this->canattend = 1;
        $this->assign_instant_credit = 0;
        $this->capacity = 0;
    }
    
    /**
     * @Column(name="id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="event_type", type="string", length=255, nullable=true) */
    protected $type_id;

    /** @Column(name="event_subtype", type="string", length=255, nullable=true) */
    protected $subtype;

    /** @Column(name="event_location", type="string", length=255, nullable=true) */
    protected $location;

    /** @Column(name="event_description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="event_starttime", type="integer", length=4) */
    protected $starttime;

    /** @Column(name="event_endtime", type="integer", length=4) */
    protected $endtime;

    /** @Column(name="event_capacity", type="integer", length=4) */
    protected $capacity;

    /** @Column(name="event_canattend", type="integer", length=1) */
    protected $canattend;

    /** @Column(name="assign_instant_credit", type="integer", length=1, nullable=true) */
    protected $assign_instant_credit;
    
    /**
     * @ManyToOne(targetEntity="Entity\EventType", inversedBy="events")
     * @JoinColumn(name="event_type", referencedColumnName="id")
     */
    protected $type;
    
    /** @OneToMany(targetEntity="Entity\EventAttendee", mappedBy="event", cascade={"remove"}) */
    protected $attendees;
    
    public function setTypeId($type_id)
    {
        if ($type_id != 0 && $type_id != $this->type_id)
            $this->type = \Entity\EventType::find($type_id);
    }
    
    /**
     * Utility Functions
     */
    
    public function getTitle()
    {
        $type_name = $this->type->name;
        
        if ($this->subtype)
            return $type_name.': '.$this->subtype;
        else
            return $type_name;
    }
    
    public function getDateText($lines = 1)
    {
        $s_date = date('F j, Y', $this->starttime);
        $e_date = date('F j, Y', $this->endtime);
        $s_time = date('g:ia', $this->starttime);
        $e_time = date('g:ia', $this->endtime);
        
        $is_same_day = (strcmp($s_date, $e_date) == 0);
        
        if ($lines == 1)
            return ($is_same_day) ?
                $s_date." - ".$s_time." to ".$e_time :
                $s_date." at ".$s_time." to ".$e_date." at ".$e_time;
        else
            return ($is_same_day) ?
                $s_date."<br />".$s_time." - ".$e_time :
                $s_date." ".$s_time." -<br />".$e_date." ".$e_time;
    }
    
    public function getAttendanceText()
    {
        $current = count($this->attendees);
        $total = ($this->capacity) ? (int)$this->capacity : '&infin;';
        
        return $current.' / '.$total;
    }
    
    public function getRelativeTime()
    {
        $today_start = time()-86400;
        $today_end = time()+86400;
        
        if ($this->starttime <= $today_end && $this->endtime >= $today_start)
            return 'today';
        else if ($this->starttime < $today_start)
            return 'past';
        else
            return 'future';
    }
    
    public function isFull()
    {
        if ($this->capacity == 0)
            return FALSE;
        else
            return (count($this->attendees) > (int)$this->capacity);
    }
    
    public function canAttend()
    {
        if ($this->isFull() || ($this->starttime < time()))
            return false;
        else if (\DF\Auth::isLoggedIn())
            return ($this->canattend == 1);
        else
            return false;
    }
}